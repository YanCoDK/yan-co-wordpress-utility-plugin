<?php

add_action('admin_init', 'yanco_utilities_register_setting');
function yanco_utilities_register_setting()
{
    register_setting('yanco-utilities-settings', 'replace_staging_images_enabled');
    register_setting('yanco-utilities-settings', 'replace_staging_images_staging_url');
    register_setting('yanco-utilities-settings', 'replace_staging_images_live_url');

    register_setting('yanco-utilities-settings', 'remove_dashboard_widgets_enabled');

    register_setting('yanco-utilities-settings', 'enable_stripe_data_for_orders_and_subscriptions');
    register_setting('yanco-utilities-settings', 'enable_action_scheduler_high_volume_processing');
    register_setting('yanco-utilities-settings', 'yanco_ashp_batch_size_factor');
    register_setting('yanco-utilities-settings', 'yanco_ashp_concurrent_batches_factor');
    register_setting('yanco-utilities-settings', 'yanco_ashp_increase_timeout_factor');
    register_setting('yanco-utilities-settings', 'yanco_ashp_queue_runner_time_limit');
    register_setting('yanco-utilities-settings', 'yanco_ashp_action_scheduler_failure_period');
    register_setting('yanco-utilities-settings', 'enable_action_scheduler_automatic_purging');
    register_setting('yanco-utilities-settings', 'yanco_action_scheduler_automatic_purging_seconds');

}

add_action('admin_menu', 'add_admin_menu_yanco_utilities');
function add_admin_menu_yanco_utilities()
{
    add_submenu_page('options-general', 'Yan&Co Utilities', 'Yan&Co Utilities', 'manage_options', 'yanco-', '');

    add_options_page(
        'Yan&Co Utilities', // page <title>Title</title>
        'Yan&Co Utilities', // menu link text
        'manage_options', // capability to access the page
        'yanco-utilities', // page URL slug
        'yanco_utilities_content', // callback function with content
        200 // priority
    );
}

function yanco_utilities_content()
{
    $table_style = 'background-color: #fff; border: 1px solid #222; padding: 0px 20px 20px 20px !important; border-collapse: initial;';


    echo '<div class="wrap">';
    echo '<h1 class="wp-heading-inline">' . __('Yan&Co Utilities', 'yanco_child_theme') . '</h1>';
    echo '<form method="post" action="options.php">';
    settings_fields('yanco-utilities-settings');
    do_settings_sections('yanco-utilities-settings');

    echo '<table class="form-table" style="'.$table_style.'">';
    echo '<tr valign="top">';
    echo '<td scope="row">';
    echo '<h2>Remove Dashboard Widgets for all users?</h2>';
    echo '<p>If enabled this setting will remove WordPress and plugins dashboard widgets for all users<br>
			this is particularly helpful to reduce the number of outbound calls from the WordPress Dashboard</p>';
    echo '</td>';
    echo '</tr>';
    echo '<tr>';
    echo '<td>';
    if (esc_attr(get_option('remove_dashboard_widgets_enabled')) == true) {
        echo '<input type="checkbox" name="remove_dashboard_widgets_enabled" checked/>';
    } else {
        echo '<input type="checkbox" name="remove_dashboard_widgets_enabled"/>';
    }
    echo '<strong style="margin-left: 10px;">Enable remove all dashboard widgets</strong></td>';
    echo '</tr>';
    echo '</table>';

    echo '<table class="form-table" style="'.$table_style.'">';
    echo '<tr valign="top">';
    echo '<td scope="row">';
    echo '<h2>Dynamic replacement of images URLs</h2>';
    echo '<p>If enabled the Staging URL will be replaced dynamically with the Live URL.<br>
			This is particularly useful if you want to use the images from the LIVE server so they don\'t have to be stored twice<br>
			<strong>Note:</strong> the URL stay as they are in the database.</p>';
    echo '</td>';
    echo '</tr>';
    echo '<tr>';
    echo '<td>';
    if (esc_attr(get_option('replace_staging_images_enabled')) == true) {
        echo '<input type="checkbox" name="replace_staging_images_enabled" checked/>';
    } else {
        echo '<input type="checkbox" name="replace_staging_images_enabled"/>';
    }
    echo '<strong style="margin-left: 10px;">Enable dynamic replacement of images URLs</strong>';
    echo '</tr>';

    echo '<tr valign="top">';
    echo '<td>';
    echo '<strong style="margin-right: 10px;">Staging URL:</strong>';
    echo '<input style="width: 100%" type="text" name="replace_staging_images_staging_url" value="' . esc_attr(get_option('replace_staging_images_staging_url')) . '" />';
    echo '</td>';
    echo '</tr>';

    echo '<tr valign="top">';
    echo '<td>';
    echo '<strong style="margin-right: 10px;">Live URL:</strong>';
    echo '<input style="width: 100%" type="text" name="replace_staging_images_live_url" value="' . esc_attr(get_option('replace_staging_images_live_url')) . '" />';
    echo '</td>';
    echo '</tr>';
    echo '</tr>';
    echo '</table>';

    if (yanco_is_plugin_active('woocommerce/woocommerce.php') && yanco_is_plugin_active('woocommerce-gateway-stripe/woocommerce-gateway-stripe.php')) {
        echo '<table class="form-table" style="'.$table_style.'">';
        echo '<tr valign="top">';
        echo '<td scope="row">';
        echo '<h2>Stripe Data for Orders and Subscriptions</h2>';
        echo '<p>If enabled this setting will display the customers stripe data and an easy link to view the data in the Stripe Dashboard.</p>';
        echo '</td>';
        echo '<tr>';
        echo '<td>';
        if (esc_attr(get_option('enable_stripe_data_for_orders_and_subscriptions')) == true) {
            echo '<input type="checkbox" name="enable_stripe_data_for_orders_and_subscriptions" checked/>';
        } else {
            echo '<input type="checkbox" name="enable_stripe_data_for_orders_and_subscriptions"/>';
        }

        echo '<strong style="margin-left: 10px;">Enable Stripe data for orders and subscriptions</strong>';
        echo '</td>';


        echo '</tr>';
        echo '</table>';
    }


    if (yanco_is_plugin_active('woocommerce/woocommerce.php')) {
        echo '<table class="form-table" style="'.$table_style.'">';
        echo '<tr valign="top">';
        echo '<td scope="row">';
        echo '<h2>Action Scheduler High Volume Processing</h2>';
        echo '<p>If enabled this adjust the settings for the Action Scheduler processing for higher throughput.</p>';
        echo '</td>';
        echo '<tr>';
        echo '<td>';
        if (esc_attr(get_option('enable_action_scheduler_high_volume_processing')) == true) {
            echo '<input type="checkbox" name="enable_action_scheduler_high_volume_processing" checked/>';
        } else {
            echo '<input type="checkbox" name="enable_action_scheduler_high_volume_processing"/>';
        }

        echo '<strong style="margin-left: 10px;">Enable action scheduler high volume processing</strong>';
        echo '</td>';

        echo '<tr valign="top">';
        echo '<td>';
        echo '<strong style="margin-right: 10px;">Batch Size Factor:</strong>';

        $yanco_ashp_batch_size_factor = get_option('yanco_ashp_batch_size_factor');
        if (empty($yanco_ashp_batch_size_factor)) {
            $yanco_ashp_batch_size_factor = 4;
        }

        echo '<input style="width: 100%;" type="number" step="1" min="1" name="yanco_ashp_batch_size_factor" value="' . esc_attr($yanco_ashp_batch_size_factor) . '" />';
        echo '</td>';
        echo '</tr>';

        echo '<tr valign="top">';
        echo '<td>';
        echo '<strong style="margin-right: 10px;">Concurrent Batches Factor:</strong>';

        $yanco_ashp_concurrent_batches_factor = get_option('yanco_ashp_concurrent_batches_factor');
        if (empty($yanco_ashp_concurrent_batches_factor)) {
            $yanco_ashp_concurrent_batches_factor = 4;
        }

        echo '<input style="width: 100%;" type="number" step="1" min="1" name="yanco_ashp_concurrent_batches_factor" value="' . esc_attr($yanco_ashp_concurrent_batches_factor) . '" />';
        echo '</td>';
        echo '</tr>';

        echo '<tr valign="top">';
        echo '<td>';
        echo '<strong style="margin-right: 10px;">Increase Timeout Factor:</strong>';

        $yanco_ashp_increase_timeout_factor = get_option('yanco_ashp_increase_timeout_factor');
        if (empty($yanco_ashp_increase_timeout_factor)) {
            $yanco_ashp_increase_timeout_factor = 6;
        }

        echo '<input style="width: 100%;" type="number" step="1" min="1" name="yanco_ashp_increase_timeout_factor" value="' . esc_attr($yanco_ashp_increase_timeout_factor) . '" />';
        echo '</td>';
        echo '</tr>';

        echo '<tr valign="top">';
        echo '<td>';
        echo '<strong style="margin-right: 10px;">Queue Runner Timelimit (in seconds):</strong>';

        $yanco_ashp_queue_runner_time_limit = get_option('yanco_ashp_queue_runner_time_limit');
        if (empty($yanco_ashp_queue_runner_time_limit)) {
            $yanco_ashp_queue_runner_time_limit = 300;
        }

        echo '<input style="width: 100%;" type="number" step="1" min="30" name="yanco_ashp_queue_runner_time_limit" value="' . esc_attr($yanco_ashp_queue_runner_time_limit) . '" />';
        echo '</td>';
        echo '</tr>';

        echo '<tr valign="top">';
        echo '<td>';
        echo '<strong style="margin-right: 10px;">Action Scheduler Failure Limit (in seconds):</strong>';

        $yanco_ashp_action_scheduler_failure_period = get_option('yanco_ashp_action_scheduler_failure_period');
        if (empty($yanco_ashp_action_scheduler_failure_period)) {
            $yanco_ashp_action_scheduler_failure_period = 300;
        }

        echo '<input style="width: 100%;" type="number" step="1" min="30" name="yanco_ashp_action_scheduler_failure_period" value="' . esc_attr($yanco_ashp_action_scheduler_failure_period) . '" />';
        echo '</td>';
        echo '</tr>';


        echo '</tr>';
        echo '</table>';
    }

    if (yanco_is_plugin_active('woocommerce/woocommerce.php')) {
        echo '<table class="form-table" style="'.$table_style.'">';
        echo '<tr valign="top">';
        echo '<td scope="row">';
        echo '<h2>Action Scheduler Automatic Purge (Default: 2592000, minimum 86400 [1 day]) </h2>';
        echo '<p>If enabled this adjust the settings for the Action Scheduler automatic purging.</p>';
        echo '</td>';
        echo '<tr>';
        echo '<td>';
        if (esc_attr(get_option('enable_action_scheduler_automatic_purging')) == true) {
            echo '<input type="checkbox" name="enable_action_scheduler_automatic_purging" checked/>';
        } else {
            echo '<input type="checkbox" name="enable_action_scheduler_automatic_purging"/>';
        }

        echo '<strong style="margin-left: 10px;">Enable change of action scheduler automatic purging</strong>';
        echo '</td>';

        echo '<tr valign="top">';
        echo '<td>';
        echo '<strong style="margin-right: 10px;">Purge after seconds:</strong>';

        $yanco_action_scheduler_automatic_purging_seconds = get_option('yanco_action_scheduler_automatic_purging_seconds');
        if (empty($yanco_action_scheduler_automatic_purging_seconds)) {
            $yanco_action_scheduler_automatic_purging_seconds = 2592000;
        }

        echo '<input style="width: 100%;" type="number" step="1" min="86400" name="yanco_action_scheduler_automatic_purging_seconds" value="' . esc_attr($yanco_action_scheduler_automatic_purging_seconds) . '" />';
        echo '</td>';
        echo '</tr>';
        echo '</table>';
    }































    submit_button();
    echo '</form>';

    echo '<table class="form-table" style="'.$table_style.'">';
    echo '<tr>';
    echo '<td>';
    echo '<h2>Activate Licenses</h2>';
    echo '<p>This will activate the licenses for the following plugins</p>';
    echo '<ul style="list-style:disc; margin-left: 15px;">';
    echo '<li>Elementor Pro</li>';
    echo '<li>Astra Pro</li>';
    echo '<li>Ultimate Addons for Elementor</li>';
    echo '<li>Gravity Forms</li>';
    echo '<li>Advanced Custom Fields Pro</li>';
    echo '<li>Admin Columns Pro (Needs to be activated through <a href="' . get_site_url() . '/wp-admin/options-general.php?page=codepress-admin-columns&tab=settings">The ACP Settings</a>)</li>';
    echo '<li>Dynamic Content for Elementor</li>';
    echo '</ul>';
    echo '<a href="' . get_site_url() . '/wp-admin/options-general.php?page=yanco-utilities&activate-yanco-licenses" class="button button-primary">Activate Licenses</a>';
    echo '</div>';
    echo '</td>';
    echo '</tr>';
    echo '</table>';

    echo '<table class="form-table" style="'.$table_style.'">';
    echo '<tr>';
    echo '<td>';
    echo '<h2>Setup Disable Emails</h2>';
    $plugins = get_plugins();

    if (!isset($plugins['disable-emails/disable-emails.php'])) {
        echo '<p><strong style="color:red;">WARNING:</strong> The <a href="https://wordpress.org/plugins/disable-emails/" rel="noopener" target="_blank">Disable Emails</a> plugin is not installed. If this is a staging make sure it\'s installed and activated</p>';
    } else {
        if (!is_plugin_active('disable-emails/disable-emails.php')) {
            echo '<p>This will activate the Disable Emails plugin</p>';
            echo '<a href="' . get_site_url() . '/wp-admin/options-general.php?page=yanco-utilities&activate-disable-emails" class="button button-primary">Activate Disable Emails</a>';
        } else {
            echo '<p>Perfectomundo! Disable Emails is activated</p>';
        }
    }

    if (isset($_GET['activate-yanco-licenses'])) {
        // Activate Astra
        if (function_exists('init_bsf_core') && class_exists('BSF_License_Manager')) {
            $bsf_license_manager = new BSF_License_Manager();
            init_bsf_core();

            $_POST = array(
                'bsf_license_manager' => array(
                    'license_key' => '74d1690d79bf362d7cf737af58b26567',
                    'product_id'  => 'astra-addon',
                ),
            );

            $_POST['bsf_activate_license'] = true;
            $_POST['bsf_graupi_nonce']     = wp_create_nonce('bsf_license_activation_deactivation_nonce');
            $bsf_license_manager->bsf_activate_license();
        }

        // Activate Ultimate Add ons for Elementor
        if (function_exists('init_bsf_core') && class_exists('BSF_License_Manager')) {
            $bsf_license_manager = new BSF_License_Manager();
            init_bsf_core();

            $_POST = array(
                'bsf_license_manager' => array(
                    'license_key' => '607851a5298755165040fd1d663b61ab',
                    'product_id'  => 'uael',
                ),
            );

            $_POST['bsf_activate_license'] = true;
            $_POST['bsf_graupi_nonce']     = wp_create_nonce('bsf_license_activation_deactivation_nonce');
            $bsf_license_manager->bsf_activate_license();
        }

        // Activate Elementor Pro
        if (class_exists('ElementorPro\License\API') && class_exists('ElementorPro\License\Admin')) {
            $elementor_license_data = ElementorPro\License\API::activate_license('f297ce319c4ab3e2bfa645b743ce3776');
            ElementorPro\License\Admin::set_license_key('f297ce319c4ab3e2bfa645b743ce3776');
            ElementorPro\License\API::set_license_data($elementor_license_data);
        }

        // Activate Gravity Forms
        if (class_exists('GFCommon')) {
            update_option('rg_gforms_key', md5('3562c5c9ed9a5c2c548816edfc5ca819'));
            // Update the contents of the gform_version_info option.
            GFCommon::get_version_info(false);
            // Update the contents of the rg_gforms_message option.
            GFCommon::cache_remote_message();
        }

        // Activate Advanced Custom Fields Pro
        if (class_exists('acf_pro')) {
            // connect
            $post = [
                'acf_license' => 'b3JkZXJfaWQ9MzM1ODJ8dHlwZT1kZXZlbG9wZXJ8ZGF0ZT0yMDE0LTA3LTA4IDA4OjUxOjQ5',
                'acf_version' => acf_get_setting('version'),
                'wp_name'     => get_bloginfo('name'),
                'wp_url'      => home_url(),
                'wp_version'  => get_bloginfo('version'),
                'wp_language' => get_bloginfo('language'),
                'wp_timezone' => get_option('timezone_string'),
            ];

            // connect
            $response = \acf_updates()->request('v2/plugins/activate?p=pro', $post);
            if ($response['status'] == 1) {
                \acf_pro_update_license($response['license']); // update license
                \acf_updates()->refresh_plugins_transient();
            }
        }

        // Activate Dynamic Content for Elementor
        if (class_exists('DynamicContentForElementor\Dashboard\License')) {
            $dynamic_ooo_license_key = 'dce-a9c8bd52-10cc97dd-59f858d8';

            update_option('dce_license_key', $dynamic_ooo_license_key);
            $res = DynamicContentForElementor\LicenseSystem::call_api('activate', $dynamic_ooo_license_key, true);

            $protocol = (!empty($_SERVER['HTTPS']) && 'off' !== $_SERVER['HTTPS'] || (!empty($_SERVER['SERVER_PORT']) && 443 === $_SERVER['SERVER_PORT'])) ? 'https://' : 'http://';
            $dce_instance = str_replace($protocol, '', get_bloginfo('wpurl'));

            if ($res) {
                update_option('dce_license_activated', 1);
                update_option('dce_license_domain', base64_encode($dce_instance));
            } else {
                update_option('dce_license_activated', 0);
                $dynamic_ooo_license_key = '';
            }
        }

        // Activate Jet Plugins
        if (class_exists('Jet_Dashboard\License_Manager')) {
            $jet_dashboard = new Jet_Dashboard\License_Manager();
            $license_action = 'activate';
            $license_key = '404d7fdf283cf9fb34b5cb393ee3a056';
            $response = $jet_dashboard->license_action_query($license_action . '_license', $license_key);

            if (isset($response['data'])) {
                $response_data = $response['data'];
            }

            $response_data = $jet_dashboard->maybe_modify_tm_responce_data($response_data);
            $jet_dashboard->update_license_list($license_key, $response_data);
        }

        // Activate Yan&Co Utilities Plugin
        if (class_exists('WC_AM_Client_2_8_1')) {
            $wcam_lib = new WC_AM_Client_2_8_1(YANCO_ROOT_PLUGIN_FILE, YANCO_UTILITIES_SOFTWARE_ID, YANCO_UTILITIES_PLUGIN_HEADER_VERSION, 'plugin', 'https://plugins.yanco.dk/', YANCO_UTILITIES_PLUGIN_HEADER_NAME);

            $args = array(
                'api_key' => '9d01f69d749db4e99ef909ad81304938d412e499',
            );

            $activation_result = $wcam_lib->activate($args);

            if (!empty($activation_result)) {
                $activate_results = json_decode($activation_result, true);
                $wc_am_activated_key = 'wc_am_client_' . YANCO_UTILITIES_SOFTWARE_ID . '_activated';
                $wc_am_deactivate_checkbox_key = 'wc_am_client_' . YANCO_UTILITIES_SOFTWARE_ID . '_deactivate_checkbox';
                $wcam_data = get_option('wc_am_client_' . YANCO_UTILITIES_SOFTWARE_ID);

                if ($activate_results['success'] === true && $activate_results['activated'] === true) {
                    add_settings_error('activate_text', 'activate_msg', sprintf(__('%s activated. ', 'yan-co-wordpress-utility-plugin'), esc_attr(YANCO_UTILITIES_PLUGIN_HEADER_NAME)) . esc_attr("{$activate_results['message']}."), 'updated');
                    update_option($wc_am_activated_key, 'Activated');
                    update_option($wc_am_deactivate_checkbox_key, 'off');
                }

                if ($activate_results == false && !empty($wcam_data) && !empty($wc_am_activated_key)) {
                    add_settings_error('api_key_check_text', 'api_key_check_error', esc_html__('Connection failed to the License Key API server. Try again later. There may be a problem on your server preventing outgoing requests, or the store is blocking your request to activate the plugin/theme.', 'yan-co-wordpress-utility-plugin'), 'error');
                    update_option($wc_am_activated_key, 'Deactivated');
                }

                if (isset($activate_results['data']['error_code']) && !empty($wcam_data) && !empty($wc_am_activated_key)) {
                    add_settings_error('wc_am_client_error_text', 'wc_am_client_error', esc_attr("{$activate_results['data']['error']}"), 'error');
                    update_option($wc_am_activated_key, 'Deactivated');
                }
            } else {
                add_settings_error('not_activated_empty_response_text', 'not_activated_empty_response_error', esc_html__('The API Key activation could not be commpleted due to an unknown error possibly on the store server The activation results were empty.', 'yan-co-wordpress-utility-plugin'), 'updated');
            }
        }
    }

    if (isset($_GET['activate-disable-emails'])) {
        activate_plugin('disable-emails/disable-emails.php');
        echo '<p>Perfectomundo! Disable Emails is activated</p>';
    }

    echo '</td>';
    echo '</tr>';
    echo '</table>';
}

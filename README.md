### Version 2.5.6 - 2024-11-28
* New: Update WCAM library to version 2.9.3

### Version 2.5.5 - 2023-10-12
* Fix: Remove extra slash for includes/display-environment-type/css/admin.css

### Version 2.5.4 - 2023-09-11
* Fix: Use !class_exists('WC_AM_Client_2_9_1')

### Version 2.5.3 - 2023-09-01
* New: Update WCAM library to version 2.9.1

### Version 2.5.2 - 2023-05-23
* New: Update WCAM library to version 2.9
* Tweak: Remove more dashboard widgets

### Version 2.5.1 - 2023-05-08
* Fix: enable_action_scheduler_high_volume_processing option name
* New: Action Scheduler Automatic Purge timing
* Tweak: Remove more dashboard widgets

### Version 2.5.0 - 2023-03-27
* New: Display Environment Type

### Version 2.4.2 - 2023-01-30
* Tweak: Handle subscriptions that have been set to cancelled status

### Version 2.4.1 - 2023-01-12
* Tweak: QuickPay MobilePay Subscription handling for subscription trial end

### Version 2.4.0 - 2023-01-09
* Fix: wpdb prepare statements

### Version 2.3.9 - 2023-01-01
* Tweak: Handle QuickPay and manual renewals

### Version 2.3.8 - 2022-12-09
* Tweak: Modifications to 'subscription trial end' failed actions

### Version 2.3.7 - 2022-12-02
* Tweak: Improvements to failed actions where susbcription is on-hold and has a pending renewal order
* New: Handle 'subscription end of prepaid term' failed actions
* Tweak: Improvements to data output for Zapier to better be able to use the Zap(s) in a meaningful way

### Version 2.3.6 - 2022-11-22
* New: Handle failures where the subscription status is on-hold and renewal order status is pending payment

### Version 2.3.5 - 2022-11-16
* Security enhancement: Use prepare for all queries
* New: Handle failures where the subscription was set to requires manual renewal and Stripe data is on the subscription

### Version 2.3.4 - 2022-11-14
* New: Handle failures for 'subscription trial end' where the subscription is 'on-hold' and there's a 'pending' renewal order, as that indicated that the payment probably failed due to insufficient funds or a blocked card. Then we requeue the action.

### Version 2.3.3 - 2022-11-14
* Tweak: Show a default message for actions

### Version 2.3.2 - 2022-11-11
* New: List actions which hasn't been auto corrected

### Version 2.3.1 - 2022-11-08
* New: ATTEMPT_AUTO_CORRECT_OF_FAILED_SUBSCRIPTIONS now handles failures for 'subscription trial end' and requests a process renewal action if there isn't any renewal orders for the subscription already

### Version 2.3.0 - 2022-09-05
* New: You can set ATTEMPT_AUTO_CORRECT_OF_FAILED_SUBSCRIPTIONS to true in wp-config.php to setup automatic requeues of actions that have failed.
* New: Option to enable viewing customers Stripe data on Orders and Subscriptions with an easy link to view the data in the Stripe Dashboard
* New: Introduced styled sections in plugin settings for better overview.
* New: Option to enable Action Scheduler High Volume Processing

### Version 2.2.1 - 2022-08-24
* Tweak: Modified REST API endpoint for WooCommerce Subscriptions failed actions output

### Version 2.2.0 - 2022-08-24
* New: Added REST API endpoint for WooCommerce Subscriptions failed actions
* Tweak: Remove WordPress Dashboard Widgets

### Version 2.1.2 - 2022-04-04
* Bugfix: Earlier declaration of define
* Bugfix: Added required permission_callback to mail log REST route

### Version 2.1.0 - 2022-02-14
* Update LSCache Config
* Removed LSCache from setup command, must now be run using configure --litespeed-cache --environment="production"
* Removed iThemes Security from setup command, must now be run using configure --ithemes-security
* Added setup of Display Environment Type to configure command
* Renamed "searchreplace" command to "searep"
* Added configuration of Enfold to "searep" command

### Version 1.2.0 - 2019-09-17
* New: Added shortcodes
* Tweak: Disable new user notification emails to admins

### Version 1.1.2 - 2019-08-16
* Fix: Reversed if for checking for Disable Emails plugin

### Version 1.1.1 - 2019-08-16
* Fix: wrong function name for is_plugin_active

### Version 1.1.0 - 2019-08-16
* Add detection and admin notice/warning for when Disable Emails plugin is activated

### Version 1.0.0 - 2019-07-15
* Enhanced Password Reset Visibility
* Remove Capital-P-Dang-It filters
* Stop defer of WooCommerce transactional emails
* Stop notifications to the blog admin of a user changing password.
* Stop notifications to the blog admin of new users
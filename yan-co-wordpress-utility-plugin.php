<?php

/*
* Plugin Name: Yan&Co WordPress Utility Plugin
* Plugin URI: https://www.yanco.dk/
* Description: This plugin adds utilities for: Site Management, Enhancing the Password Reset Visibility for WordPress login forms, Disables New User and Changed Password Email notifications, Disables the WooCommerce Defer Transactional Emails so emails are sent right away.
* Version: 2.5.6
* Author: Yan&Co
* Author URI: https://www.yanco.dk
* License: Copyright Yan&Co
* Text Domain: yan-co-wordpress-utility-plugin
*/

define('YANCO_UTILITIES_SOFTWARE_ID', '16580');
define('YANCO_UTILITIES_PLUGIN_HEADER_NAME', 'Yan&Co WordPress Utility Plugin');
define('YANCO_UTILITIES_PLUGIN_HEADER_VERSION', '2.5.6');
define('YANCO_ROOT_PLUGIN_FILE', __FILE__);
define('YANCO_UTILITY_PLUGIN_DIR_PATH', plugin_dir_path(__FILE__));

// Exit if accessed directly
if (!defined('ABSPATH')) {
    exit;
}

if (!class_exists('WCDependencies')) {
    require_once(dirname(__FILE__) . '/includes/WCDependencies.php');
}

if (!class_exists('YanCoUtilityHelper')) {
    require_once(dirname(__FILE__) . '/includes/YanCoUtilityHelper.php');
}

if (!class_exists('YanCoLicenses')) {
    require_once(dirname(__FILE__) . '/includes/YanCoLicenses.php');
}

function yanco_disable_emails_activated_notice()
{
    $admin_notice_content = 'The plugin <strong>Disable Emails</strong> <em>(disable-emails/disable-emails.php)</em> is active. No emails will be sent out from WordPress.';
    echo '<div class="error"><p>';
    echo $admin_notice_content;
    echo '</p></div>';
}

if (yanco_is_plugin_active('disable-emails/disable-emails.php')) {
    add_action('admin_notices', 'yanco_disable_emails_activated_notice');
}

function yanco_utilities_is_possible_wcam_activation_problem()
{
    $is_possible_wcam_activation_problem = false;

    if (defined(('YANCO_UTILITIES_SOFTWARE_ID'))) {
        $wc_am_client_instance_option_exists = get_option('wc_am_client_' . YANCO_UTILITIES_SOFTWARE_ID . '_instance');
        if ($wc_am_client_instance_option_exists === false) {
            $is_possible_wcam_activation_problem = true;
        }
    }

    return $is_possible_wcam_activation_problem;
}

function yanco_utilities_get_possible_wcam_activation_problem_notice()
{
    $plugin_name = '<strong>Yan&Co WordPress Utility Plugin</strong>';
    $plugins_admin_url = get_admin_url() . 'plugins.php';
    $admin_notice_content = '';
    $admin_notice_content .= '<div class="error"><p>';
    $admin_notice_content .= 'A possible activation problem has been detected with the plugin ' . $plugin_name . '. Please go to <a href="' . $plugins_admin_url . '">' . $plugins_admin_url . '</a>, deactivate ' . $plugin_name . ' and then reactivate ' . $plugin_name . '.';
    $admin_notice_content .= '</p></div>';

    echo $admin_notice_content;
}

// Load WC_AM_Client class if it exists.
if (!class_exists('WC_AM_Client_2_9_3')) {
    /*
     * |---------------------------------------------------------------------
     * | This must be exactly the same for both plugins and themes.
     * |---------------------------------------------------------------------
     */
    require_once(YANCO_UTILITY_PLUGIN_DIR_PATH . 'includes/am/wc-am-client.php');
}

$is_possible_wcam_activation_problem = yanco_utilities_is_possible_wcam_activation_problem();
if ($is_possible_wcam_activation_problem === true) {
    add_action('admin_notices', 'yanco_utilities_get_possible_wcam_activation_problem_notice');
}

// Instantiate WC_AM_Client class object if the WC_AM_Client class is loaded.
if (class_exists('WC_AM_Client_2_9_3')) {
    /**
     * This file is only an example that includes a plugin header, and this code used to instantiate the client object. The variable $wcam_lib
     * can be used to access the public properties from the WC_AM_Client class, but $wcam_lib must have a unique name. To find data saved by
     * the WC_AM_Client in the options table, search for wc_am_client_{product_id}, so in this example it would be wc_am_client_132967.
     *
     * All data here is sent to the WooCommerce API Manager API, except for the $software_title, which is used as a title, and menu label, for
     * the API Key activation form the client will see.
     *
     * ****
     * NOTE
     * ****
     * If $product_id is empty, the customer can manually enter the product_id into a form field on the activation screen.
     *
     * @param string $file             Must be __FILE__ from the root plugin file, or theme functions, file locations.
     * @param int    $product_id       Must match the Product ID number (integer) in the product.
     * @param string $software_version This product's current software version.
     * @param string $plugin_or_theme  'plugin' or 'theme'
     * @param string $api_url          The URL to the site that is running the API Manager. Example: https://www.toddlahman.com/ Must be the root URL.
     * @param string $software_title   The name, or title, of the product. The title is not sent to the API Manager APIs, but is used for menu titles.
     *
     * Example:
     *
     * $wcam_lib = new WC_AM_Client_2_9_3( $file, $product_id, $software_version, $plugin_or_theme, $api_url, $software_title );
     */

    $wcam_lib = new WC_AM_Client_2_9_3(
        YANCO_ROOT_PLUGIN_FILE,
        YANCO_UTILITIES_SOFTWARE_ID,
        YANCO_UTILITIES_PLUGIN_HEADER_VERSION,
        'plugin',
        'https://plugins.yanco.dk/',
        YANCO_UTILITIES_PLUGIN_HEADER_NAME,
        'yanco_wordpress_utility_plugin'
    );
}

yanco_wordpress_utility_plugin_init();

// This code will not run until the API Key is activated.
function yanco_wordpress_utility_plugin_init()
{
    return Yan_Co_WordPress_Utility_Plugin_Main_Class::instance();
}

class Yan_Co_WordPress_Utility_Plugin_Main_Class
{
    public $text_domain = '';

    /**
     * @var The single instance of the class
     */
    protected static $_instance = null;

    public static function instance()
    {
        if (is_null(self::$_instance)) {
            self::$_instance = new self();
        }

        return self::$_instance;
    }

    /**
     * Cloning is forbidden.
     *
     * @since 1.2
     */
    public function __clone()
    {
    }

    /**
     * Unserializing instances of this class is forbidden.
     *
     * @since 1.2
     */
    public function __wakeup()
    {
    }

    public function __construct()
    {
        $this->text_domain = 'yan-co-wordpress-utility-plugin';

        // Ready for translation
        load_plugin_textdomain($this->text_domain, false, dirname(untrailingslashit(plugin_basename(__FILE__))) . '/languages');

        add_action('login_enqueue_scripts', array($this, 'yanco_enhance_wp_login_button_visibility'));

        if (is_woocommerce_active()) {
            add_action('woocommerce_defer_transactional_emails', array($this, 'yanco_woocommerce_defer_transactional_emails'));
        }

        add_action('init', array($this, 'yanco_det_add_hooks'));
        add_action('plugins_loaded', array($this, 'yanco_disable_capital_p_dang_it_init'));

        // Handles activations
        register_activation_hook(__FILE__, array($this, 'yanco_utilities_deactivate_display_environment_type' ));

        // Handle updates
        add_action('plugins_loaded', array($this, 'yanco_utilities_deactivate_display_environment_type'));

        require_once(dirname(__FILE__) . '/includes/shortcodes.php');
        require_once(dirname(__FILE__) . '/includes/helpers.php');
        require_once(dirname(__FILE__) . '/includes/autoupdater.php');
        require_once(dirname(__FILE__) . '/admin/admin-page.php');
        require_once(dirname(__FILE__) . '/includes/staging-tools/staging-image-from-live.php');
        require_once(dirname(__FILE__) . '/includes/rest-api/rest-api-setup.php');
        require_once(dirname(__FILE__) . '/includes/rest-api/wp-mail-log.php');
        require_once(dirname(__FILE__) . '/includes/rest-api/woocommerce-subscriptions-error-monitor.php');
        require_once(dirname(__FILE__) . '/includes/display-environment-type/display-environment-type.php');

        if (esc_attr(get_option('remove_dashboard_widgets_enabled')) == true) {
            require_once(dirname(__FILE__) . '/includes/wp-dashboard/wp-dashboard.php');
        }

        if (yanco_is_plugin_active('woocommerce/woocommerce.php') && yanco_is_plugin_active('woocommerce-gateway-stripe/woocommerce-gateway-stripe.php')) {
            if (esc_attr(get_option('enable_stripe_data_for_orders_and_subscriptions')) == true) {
                require_once(dirname(__FILE__) . '/includes/woocommerce-gateway-stripe/woocommerce-gateway-stripe.php');
            }
        }

        if (yanco_is_plugin_active('woocommerce/woocommerce.php')) {
            if (esc_attr(get_option('enable_action_scheduler_high_volume_processing')) == true) {
                require_once(dirname(__FILE__) . '/includes/action-scheduler/action-scheduler.php');
            }
        }

        if (yanco_is_plugin_active('woocommerce/woocommerce.php')) {
            if (esc_attr(get_option('enable_action_scheduler_automatic_purging')) == true) {
                require_once(dirname(__FILE__) . '/includes/action-scheduler/action-scheduler-automatic-purging.php');
            }
        }

        if (defined('WP_CLI') && WP_CLI) {
            require_once(dirname(__FILE__) . '/includes/cli-commands/yanco-utilities.php');
        }

        // Allow Shortcodes in Menu Items
        add_filter('wp_nav_menu_items', 'do_shortcode');

        $yanco_ashp_action_scheduler_failure_period = esc_attr(get_option('yanco_ashp_action_scheduler_failure_period'));
        if (empty($yanco_ashp_action_scheduler_failure_period)) {
            $yanco_ashp_action_scheduler_failure_period = 300;
        }
    }

    public function yanco_enhance_wp_login_button_visibility()
    {
        echo "<script>

            function addButtonClasses() {
                var array_buttons = document.querySelectorAll('#nav a');

                array_buttons.forEach(function(button) {
                    button.className = 'button button-primary button-large';
                });

                document.querySelector('#backtoblog > a').className = 'button button-primary button-large';

                var element = document.querySelector( '.privacy-policy-page-link > a' );
                if ( typeof(element) != 'undefined' && element != null ) {
                    document.querySelector('.privacy-policy-page-link > a').className = 'button button-large';
                }
            }

            window.onload = addButtonClasses;

        </script>";

        echo "<style>
            .login #nav {
                margin: 16px 0 0 0 !important;
                color: transparent;
            }

            .login #nav,
            .login #backtoblog {
                float: left;
                padding: 0px 0px 0px 5px !important;
                width: 48%;
            }

            .login #nav a,
            .login #backtoblog a {
                font-size: 10px;
                color: #fff !important;
                width: 100%;
                text-align: center;
            }

            .login .privacy-policy-page-link {
                padding-left: 5px !important;
                padding-right: 5px !important;
                width: initial !important;
            }

            .login .privacy-policy-page-link > a {
                width: 100%;
            }

        </style>";
    }

    public function yanco_woocommerce_defer_transactional_emails()
    {
        return false;
    }

    public function yanco_disable_capital_p_dang_it_init()
    {
        remove_filter('the_title', 'capital_P_dangit', 11);
        remove_filter('the_content', 'capital_P_dangit', 11);
        remove_filter('comment_text', 'capital_P_dangit', 31);
    }

    public function yanco_det_add_hooks()
    {
        // Bail if we shouldn't display.
        if (! yanco_det_should_display()) {
            return;
        }

        // Add an item to the "at a glance" dashboard widget.
        add_filter('dashboard_glance_items', 'yanco_det_add_glance_item');

        // Add an admin bar item if in wp-admin.
        add_action('admin_bar_menu', 'yanco_det_add_toolbar_item');

        // Add styling.
        add_action('admin_enqueue_scripts', 'yanco_det_enqueue_styles');
        add_action('wp_enqueue_scripts', 'yanco_det_enqueue_styles');
    }

    public function yanco_utilities_deactivate_display_environment_type()
    {
        if (yanco_is_plugin_active('display-environment-type/display-environment-type.php')) {
            deactivate_plugins('/display-environment-type/display-environment-type.php');
        }
    }
}

// Needs to be initialized outside of class because of it being pluggable functions
require_once(dirname(__FILE__) . '/includes/pluggable-functions.php');

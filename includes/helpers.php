<?php

// Pretty print_r
if (!function_exists('_yppr')) {
    function _yppr($object)
    {
        print('<pre>' . print_r($object, true) . '</pre>');
    }
}

if (!function_exists('_ylog')) {
    function _ylog($message)
    {
        if (is_array($message) || is_object($message)) {
            error_log(print_r($message, true));
        } else {
            error_log($message);
        }
    }
}

<?php

if (get_option('replace_staging_images_enabled') == true) {
	add_filter('wp_get_attachment_url', 'yanco_utilities_staging_wp_get_attachment_url', 100000, 2);
	add_filter('wp_get_attachment_image_attributes', 'yanco_utilities_staging_wp_get_attachment_image_attributes', 100000);
	add_filter('wp_get_attachment_image_src', 'yanco_utilities_staging_wp_get_attachment_image_src', 10, 4);
	add_filter('wp_calculate_image_srcset', 'yanco_utilities_staging_wp_calculate_image_srcset', 10000);
}


function yanco_utilities_staging_wp_get_attachment_url($url, $attachment)
{
	$staging_url = get_option('replace_staging_images_staging_url');
	$live_url = get_option('replace_staging_images_live_url');

	if (empty($staging_url) || empty($live_url)) {
		return $url;
	}

	if (strpos($url, $staging_url) !== false) {
		$url = str_replace($staging_url, $live_url, $url);
	}

	return $url;
}


function yanco_utilities_staging_wp_get_attachment_image_attributes($attributes)
{
	$staging_url = get_option('replace_staging_images_staging_url');
	$live_url = get_option('replace_staging_images_live_url');

	if (empty($staging_url) || empty($live_url)) {
		return $attributes;
	}

	if (isset($attributes['src'])) {
		if (strpos($attributes['src'], $staging_url) !== false) {
			$attributes['src'] = str_replace($staging_url, $live_url, $attributes['src']);
		}
	}

	if (isset($attributes['srcset'])) {
		if (strpos($attributes['srcset'], $staging_url) !== false) {
			$attributes['srcset'] = str_replace($staging_url, $live_url, $attributes['srcset']);
		}
	}

	return $attributes;
}


function yanco_utilities_staging_wp_get_attachment_image_src($image, $attachment_id, $size, $icon)
{
	$staging_url = get_option('replace_staging_images_staging_url');
	$live_url = get_option('replace_staging_images_live_url');

	if (isset($image[0])) {
		$image[0] = str_replace($staging_url, $live_url, $image[0]);
	}

	return $image;
}

function yanco_utilities_staging_wp_calculate_image_srcset($sources)
{
	$staging_url = get_option('replace_staging_images_staging_url');
	$live_url = get_option('replace_staging_images_live_url');

	foreach ($sources as &$source) {
		$source['url'] = str_replace($staging_url, $live_url, $source['url']);
	}

	return $sources;
}

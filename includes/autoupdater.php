<?php

// Disable plugins auto-update UI elements.
add_filter( 'plugins_auto_update_enabled', '__return_false' );

// Disable themes auto-update UI elements.
add_filter( 'themes_auto_update_enabled', '__return_false' );

// Disable auto-update email notifications for plugins.
add_filter( 'auto_plugin_update_send_email', '__return_true' );

// Disable auto-update email notifications for themes.
add_filter( 'auto_theme_update_send_email', '__return_true' );

// Now, if you want to customize the label of the auto-update link text, you can use the filter as shown in the following snippet.
// add_filter( 'plugin_auto_update_setting_html', function( $html, $plugin_file, $plugin_data ){
// 	if ( 'plugin-dir/plugin-name.php' === $plugin_file ) {
// 		$html = __( 'Custom HTML', 'yan-co-wordpress-utility-plugin' );
// 	}
// 	return $html;	
// 	}, 
// 	10, 
// 	3 
// );
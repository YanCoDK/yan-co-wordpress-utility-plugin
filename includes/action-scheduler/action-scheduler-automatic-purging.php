<?php

add_filter('action_scheduler_retention_period', 'yanco_action_scheduler_purge');
/**
 * Change Action Scheduler default purge
 */
function yanco_action_scheduler_purge()
{
    $yanco_action_scheduler_automatic_purging_seconds = get_option('yanco_action_scheduler_automatic_purging_seconds');
    if (empty($yanco_action_scheduler_automatic_purging_seconds)) {
        $yanco_action_scheduler_automatic_purging_seconds = 2592000;
    }

    return $yanco_action_scheduler_automatic_purging_seconds;
}

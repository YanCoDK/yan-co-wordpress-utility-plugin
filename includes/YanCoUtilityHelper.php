<?php

/**
 * WC Dependency Checker
 *
 * Checks if WooCommerce is enabled
 */
class YanCoUtilityHelper
{
    private static $active_plugins;

    public static function init()
    {
        self::$active_plugins = (array) get_option('active_plugins', array());

        if (is_multisite()) {
            self::$active_plugins = array_merge(self::$active_plugins, get_site_option('active_sitewide_plugins', array()));
        }
    }

    public static function plugin_active_check($plugin_key)
    {
        if (!self::$active_plugins) {
            self::init();
        }

        return in_array($plugin_key, self::$active_plugins) || array_key_exists($plugin_key, self::$active_plugins);
    }
}

/**
 * WC Detection
 */
if (!function_exists('yanco_is_plugin_active')) {
    function yanco_is_plugin_active($plugin_key)
    {
        return YanCoUtilityHelper::plugin_active_check($plugin_key);
    }
}

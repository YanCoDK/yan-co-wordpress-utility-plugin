<?php

if (!shortcode_exists('yanco_current_year')) {
    add_shortcode('yanco_current_year', 'yanco_current_year_shortcode');
    function yanco_current_year_shortcode()
    {
        return date('Y');
    }
}

if( !shortcode_exists( 'yanco_login_out' ) ) {
    add_shortcode( 'yanco_login_out', 'yanco_login_out_text' );
    function yanco_login_out_text() {
        if( is_user_logged_in() ) {
            return __('Log ud', 'yanco_child_theme');
        } else {
            return __('Log ind', 'yanco_child_theme');
        }
    }
}

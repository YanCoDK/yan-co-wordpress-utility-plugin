<?php

// http://yancoutilitiesplugin.local/wp-json/wp-mail-logging/v1/errors/?apikey=48ebf204-7565-4694-95a7-ff8426e0da53
// http://yancoutilitiesplugin.local/wp-json/wp-mail-logging/v1/errors/?apikey=48ebf204-7565-4694-95a7-ff8426e0da53&testmode=1

// https://www.yanco.dk/wp-json/wp-mail-logging/v1/errors/?apikey=48ebf204-7565-4694-95a7-ff8426e0da53&LSCWP_CTRL=purge

if (yanco_is_plugin_active('wp-mail-logging/wp-mail-logging.php')) {
    add_action('rest_api_init', function () {
        register_rest_route('wp-mail-logging/v1', '/errors', array(
            'methods' => 'GET',
            'callback' => 'yanco_get_wp_mail_log_errors',
            'permission_callback' => '__return_true'
        ));
    });

    function yanco_get_wp_mail_log_errors(WP_REST_Request $request)
    {
        $apikey = $request->get_param('apikey');
        $testmode = $request->get_param('testmode');

        $result = array();

        if (!isset($apikey)) {
            return $result;
        }

        if (isset($apikey) && $apikey != YANCO_REST_API_KEY) {
            return $result;
        }

        if (isset($apikey) && $apikey == YANCO_REST_API_KEY) {
            global $wpdb;

            $results = null;

            $table = $wpdb->prefix . 'wpml_mails';
            $now = new DateTime();
            $now->setTimezone(new DateTimeZone('Europe/Copenhagen'));
            $now->modify('-15 minutes');
            $timeframe = $now->format('Y-m-d H:i:s');
            $sql = "SELECT * FROM {$table} WHERE timestamp >= '{$timeframe}' AND error IS NOT NULL";
            $db_results = $wpdb->get_results($wpdb->prepare($sql));
            $results['timeframe'] = $timeframe;

            foreach ($db_results as $row) {
                $results['errors'][] = array(
                    'timestamp' => $row->timestamp,
                    'error' => $row->error,
                );
            }

            if ($testmode == '1') {
                $results['errors'][] = array(
                    'timestamp' => date('Y-m-d H:i:s'),
                    'error' => 'test error',
                );
            }

            if (!empty($results['errors'])) {
                return new WP_Error(
                    '1000',
                    'Errors found in WP Mail Logging table',
                    array(
                        'status' => 400,
                        'errors' => $results['errors']
                    )
                );
            }

            return $results;
        }
    }
}

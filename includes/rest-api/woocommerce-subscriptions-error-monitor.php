<?php

// https://yancoutilitiesplugin.local/wp-json/wcs-monitor/v1/failed-actions/?apikey=48ebf204-7565-4694-95a7-ff8426e0da53
// https://yancoutilitiesplugin.local/wp-json/wcs-monitor/v1/failed-actions/?apikey=48ebf204-7565-4694-95a7-ff8426e0da53&testmode=1
// https://www.yanco.dk/wp-json/wcs-monitor/v1/failed-actions/?apikey=48ebf204-7565-4694-95a7-ff8426e0da53&LSCWP_CTRL=purge

if (yanco_is_plugin_active('woocommerce-subscriptions/woocommerce-subscriptions.php')) {
    add_action('rest_api_init', function () {
        register_rest_route('wcs-monitor/v1', '/failed-actions', array(
            'methods' => 'GET',
            'callback' => 'yanco_get_wcs_failed_actions',
            'permission_callback' => '__return_true'
        ));
    });

    function yanco_get_wcs_failed_actions(WP_REST_Request $request)
    {
        $apikey = $request->get_param('apikey');
        $testmode = $request->get_param('testmode');

        $results = array();

        if (!isset($apikey)) {
            return $results;
        }

        if (isset($apikey) && $apikey != YANCO_REST_API_KEY) {
            return $results;
        }

        if (isset($apikey) && $apikey == YANCO_REST_API_KEY) {
            global $wpdb;

            $results = array();
            $results['failed_actions'] = array();
            $results['failed_actions']['auto_correct_success'] = array();
            $results['failed_actions']['auto_correct_success']['message'] = '';
            $results['failed_actions']['auto_correct_success']['urls'] = array();
            $results['failed_actions']['auto_correct_success']['urls_formatted'] = '';

            $results['failed_actions']['auto_correct_failed'] = array();
            $results['failed_actions']['auto_correct_failed']['message'] = '';
            $results['failed_actions']['auto_correct_failed']['urls'] = array();
            $results['failed_actions']['auto_correct_failed']['urls_formatted'] = '';

            $woocommerce_subscriptions_failed_scheduled_actions = get_option('woocommerce_subscriptions_failed_scheduled_actions');

            if ($testmode == true) {
                $test_data = 'a:6:{i:123827;a:2:{s:4:"args";a:1:{s:15:"subscription_id";i:39182;}s:4:"type";s:20:"subscription payment";}i:137884;a:2:{s:4:"args";a:1:{s:8:"order_id";i:42864;}s:4:"type";s:26:"subscription payment retry";}i:137261;a:2:{s:4:"args";a:1:{s:15:"subscription_id";i:42824;}s:4:"type";s:20:"subscription payment";}i:154257;a:2:{s:4:"args";a:1:{s:15:"subscription_id";i:47139;}s:4:"type";s:22:"subscription trial end";}i:154102;a:2:{s:4:"args";a:1:{s:15:"subscription_id";i:47111;}s:4:"type";s:22:"subscription trial end";}i:154392;a:2:{s:4:"args";a:1:{s:15:"subscription_id";i:47295;}s:4:"type";s:22:"subscription trial end";}}';
                $woocommerce_subscriptions_failed_scheduled_actions = unserialize($test_data);
            }

            if (!empty($woocommerce_subscriptions_failed_scheduled_actions)) {
                foreach ($woocommerce_subscriptions_failed_scheduled_actions as $action_id => $failed_action) {
                    $action_subscription_id = null;
                    $action_type = $failed_action['type'];
                    $action_type_id = null;
                    $action_id = $action_id;

                    switch($failed_action['type']) {
                        case 'subscription payment':
                            $action_subscription_id = $failed_action['args']['subscription_id'];
                            $action_type_id = 1;
                            break;
                        case 'subscription payment retry':
                            $action_subscription_id = $failed_action['args']['order_id'];
                            $action_type_id = 2;
                            break;
                        case 'subscription trial end':
                            $action_subscription_id = $failed_action['args']['subscription_id'];
                            $action_type_id = 3;
                            break;
                        case 'subscription end of prepaid term':
                            $action_subscription_id = $failed_action['args']['subscription_id'];
                            $action_type_id = 4;
                            break;
                        default:
                            $action_subscription_id = $failed_action['args']['order_id'];
                            $action_type_id = 0;
                            break;
                    }

                    $url = get_site_url() . '/wp-admin/post.php?post='.$action_subscription_id.'&action=edit';
                    $results['failed_actions']['actions'][] = array(
                        'ID' => $action_subscription_id,
                        'action_id' => $action_id,
                        'type' => $action_type,
                        'type_id' => $action_type_id,
                        'url' => $url,
                    );
                }



                if (defined('ATTEMPT_AUTO_CORRECT_OF_FAILED_SUBSCRIPTIONS')) {
                    if (ATTEMPT_AUTO_CORRECT_OF_FAILED_SUBSCRIPTIONS === true) {
                        global $wpdb;

                        foreach ($results['failed_actions']['actions'] as $failed_action) {
                            $action_id = sanitize_text_field($failed_action['action_id']);
                            $action_type = sanitize_text_field($failed_action['type']);
                            $action_type_id = sanitize_text_field($failed_action['type_id']);
                            $subscription_id = sanitize_text_field($failed_action['ID']);
                            $subscription_url = sanitize_text_field($failed_action['url']);
                            $auto_correct_completed = false;

                            $posts_tablename = $wpdb->prefix . 'posts';
                            $postmeta_tablename = $wpdb->prefix . 'postmeta';
                            $actionscheduler_actions_tablename = $wpdb->prefix . 'actionscheduler_actions';

                            if ($action_type_id == 1 || $action_type_id == 2) { // subscription payment || subscription payment retry
                                if ($testmode == false) {
                                    // Try to cancel and 'wc-pending' renewals already existing on the subscription

                                    $sql_for_related_orders_with_pending_status = $wpdb->prepare(
                                        "SELECT ID FROM %1s WHERE post_status = %s AND ID IN (
                                            SELECT post_id FROM %1s WHERE meta_key = %s AND meta_value = %d
                                        )",
                                        $posts_tablename,
                                        'wc-pending',
                                        $postmeta_tablename,
                                        '_subscription_renewal',
                                        $subscription_id
                                    );

                                    $sql_for_related_orders_with_pending_status_results = $wpdb->query($sql_for_related_orders_with_pending_status);

                                    if (!empty($sql_for_related_orders_with_pending_status_results)) {
                                        foreach ($sql_for_related_orders_with_pending_status_results as $order_with_pending_status) {
                                            $post_id = $order_with_pending_status->ID;

                                            if (!empty($post_id)) {
                                                $data = array(
                                                    'ID' => $post_id,
                                                    'post_status' => 'wc-cancelled',
                                                );
                                                wp_update_post($data);
                                            }
                                        }
                                    }

                                    // Find failed action to requeue the action
                                    $sql_select_for_action = $wpdb->prepare(
                                        "SELECT * FROM %1s WHERE action_id = %d AND status = %s",
                                        $actionscheduler_actions_tablename,
                                        $action_id,
                                        'failed'
                                    );
                                    $sql_select_for_action_results_before = $wpdb->query($sql_select_for_action);

                                    if ($sql_select_for_action_results_before > 0) {
                                        // Set status to pending for failed action to requeue the action
                                        $sql_for_action = $wpdb->prepare(
                                            "UPDATE %1s SET status = %s WHERE action_id = %d AND status = %s",
                                            $actionscheduler_actions_tablename,
                                            'pending',
                                            $action_id,
                                            'failed'
                                        );
                                        $sql_for_action_results = $wpdb->query($sql_for_action);

                                        // Check there's 0 results after the update
                                        // $sql_select_for_action_results_after = $wpdb->query($sql_select_for_action);

                                        if ($sql_for_action_results > 0) {
                                            $auto_correct_completed = true;
                                        }
                                    }

                                    // Find subscription to requeue the action for
                                    $sql_select_for_subscription = $wpdb->prepare(
                                        "SELECT * FROM %1s WHERE ID = %d AND post_status = %s",
                                        $posts_tablename,
                                        $subscription_id,
                                        'wc-on-hold'
                                    );
                                    $sql_select_for_subscription_results_before = $wpdb->query($sql_select_for_subscription);

                                    if ($sql_select_for_subscription_results_before > 0) {
                                        // Set status to 'wc-active' for subscription to requeue the action
                                        $sql_for_subscription = $wpdb->prepare(
                                            "UPDATE %1s SET post_status = %s WHERE ID = %d AND post_status = %s",
                                            $posts_tablename,
                                            'wc-active',
                                            $subscription_id,
                                            'wc-on-hold'
                                        );
                                        $sql_for_subscription_results = $wpdb->query($sql_for_subscription);

                                        // Check there's 0 results after the update
                                        // $sql_select_for_subscription_results_after = $wpdb->query($sql_select_for_subscription);

                                        if ($sql_for_subscription_results > 0) {
                                            $auto_correct_completed = true;
                                        }
                                    }

                                    if ($auto_correct_completed == false) {
                                        // Check for manual renewal set to true
                                        $_requires_manual_renewal = get_post_meta($subscription_id, '_requires_manual_renewal', true);
                                        if ($_requires_manual_renewal == true) {
                                            // Check there's a stripe customer ID
                                            $_stripe_customer_id = get_post_meta($subscription_id, '_stripe_customer_id', true);
                                            $_quickpay_transaction_id = get_post_meta($subscription_id, '_quickpay_transaction_id', true);

                                            $data_title = '';
                                            if ($_stripe_customer_id) {
                                                $data_title = 'Stripe';
                                            } elseif ($_quickpay_transaction_id) {
                                                $data_title = 'QuickPay';
                                            }

                                            if ($_stripe_customer_id || $_quickpay_transaction_id) {
                                                $subscription = wcs_get_subscription($subscription_id);

                                                if ($subscription) {
                                                    $subscription->update_status('active');
                                                    $subscription->add_order_note(__('Process renewal detected that the subscription was set to requires manual renewal and '.$data_title.' data is on the subscription. Auto correction service adjusted data and processed renewal.', 'yan-co-wordpress-utility-plugin'), false, true);
                                                    update_post_meta($subscription_id, '_requires_manual_renewal', false);
                                                    do_action('woocommerce_scheduled_subscription_payment', $subscription->get_id());

                                                    $auto_correct_completed = true;
                                                }
                                            }
                                        }
                                    }
                                } else {
                                    $auto_correct_completed = true;
                                }
                            } elseif ($action_type_id == 3) { // subscription trial end
                                $subscription = wcs_get_subscription($subscription_id);

                                if ($subscription) {
                                    /*
                                    * Scenario 1: Subscription Trial End - No renewal order exists
                                    */
                                    $related_renewal_orders = $subscription->get_related_orders('ids', 'renewal');
                                    // Check there's no renewal order already as this only applies for 'subscription trial end'
                                    if (empty($related_renewal_orders)) {
                                        if ($testmode == false) {
                                            if ($subscription->get_status() == 'cancelled') {
                                                $subscription->add_order_note(__('Process renewal order cannot be requested by auto correction service as the subscription has the status Cancelled', 'yan-co-wordpress-utility-plugin'), false, true);
                                            } else {
                                                $subscription->update_status('active');
                                                $subscription->add_order_note(__('Process renewal order requested by auto correction service', 'yan-co-wordpress-utility-plugin'), false, true);
                                                do_action('woocommerce_scheduled_subscription_payment', $subscription->get_id());
                                            }
                                        }
                                        $auto_correct_completed = true;
                                    } else {
                                        /*
                                        * Scenario 2: Subscription Trial End - Renewal Orders exists
                                        */

                                        $order_id = reset($related_renewal_orders);

                                        // Probably no need to loop them all as they are all ordered with the latest at the top of the array
                                        // So we only need to look at the latest one

                                        // foreach ($related_renewal_orders as $order_id) {
                                        $order = wc_get_order($order_id);

                                        if ($order) {
                                            $order_status = $order->get_status();
                                            if ($order_status == 'cancelled') {
                                                // Do nothing
                                                // $auto_correct_completed = true;
                                                // continue;
                                            } elseif ($order_status == 'completed') {
                                                // Do nothing
                                                $auto_correct_completed = true;
                                            // continue;
                                            } elseif ($order_status == 'pending-cancel') {
                                                // Do nothing
                                                $auto_correct_completed = true;
                                            // continue;
                                            } elseif ($order_status == 'pending') {
                                                $subscription_status = $subscription->get_status();
                                                // Check the status of the subscription to see if it's on-hold
                                                if ($subscription_status == 'on-hold') {
                                                    /*
                                                    * Scenario 3: Subscription Trial End
                                                    * Subscription Status is On Hold
                                                    * Renewal Order with Status 'Pending'
                                                    */

                                                    /*
                                                    * Check that there are no pending payment retries in the action scheduler
                                                    * because if there are, WooCommerce Subscription has already setup a pending action
                                                    * to handle this.
                                                    */
                                                    $action_args = '{"order_id":'.$order->get_id() .'}';

                                                    $sql_select_for_action = $wpdb->prepare(
                                                        "SELECT * FROM %1s WHERE hook = %s AND status = %s AND args = %s",
                                                        $actionscheduler_actions_tablename,
                                                        'woocommerce_scheduled_subscription_payment_retry',
                                                        'pending',
                                                        $action_args
                                                    );
                                                    $sql_select_for_action_results_before = $wpdb->query($sql_select_for_action);

                                                    if ($sql_select_for_action_results_before > 0) {
                                                        // It's already queued for a retry, so we mark it as auto corrected
                                                        $auto_correct_completed = true;
                                                    } else {
                                                        /*
                                                        * If it's a MobilePay Subscription order
                                                        * Check that the renewal order is not older than 72 hours
                                                        */
                                                        $is_mobilepay_subscription_and_renewal_is_less_than_72_hours = false;
                                                        $subscription_payment_method = $subscription->get_payment_method();
                                                        if ($subscription_payment_method == 'mobilepay-subscriptions') {
                                                            $order_date_created = $order->get_date_created();

                                                            $now = new DateTime();
                                                            $now->setTimezone(new DateTimeZone('Europe/Copenhagen'));
                                                            $difference = $order_date_created->diff($now);
                                                            $diff_hours = ($difference->days * 24) + $difference->h;

                                                            // The reason for this check is that MobilePay Subscription orders take up to 72 hours to complete
                                                            if ($diff_hours <= 71) {
                                                                $is_mobilepay_subscription_and_renewal_is_less_than_72_hours = true;
                                                            }
                                                        }

                                                        // Check for manual renewal set to true
                                                        $_requires_manual_renewal = get_post_meta($subscription->get_id(), '_requires_manual_renewal', true);
                                                        if ($_requires_manual_renewal == true) {
                                                            // Check there's a stripe customer ID or QuickPay Transaction ID
                                                            $_stripe_customer_id = get_post_meta($subscription->get_id(), '_stripe_customer_id', true);
                                                            $_quickpay_transaction_id = get_post_meta($subscription_id, '_quickpay_transaction_id', true);

                                                            $data_title = '';
                                                            if ($_stripe_customer_id) {
                                                                $data_title = 'Stripe';
                                                            } elseif ($_quickpay_transaction_id && $subscription_payment_method != 'mobilepay-subscriptions') {
                                                                $data_title = 'QuickPay';
                                                            } elseif ($_quickpay_transaction_id && $subscription_payment_method == 'mobilepay-subscriptions') {
                                                                $data_title = 'QuickPay MobilePay Subscriptions';
                                                            }

                                                            if ($_stripe_customer_id || $_quickpay_transaction_id) {
                                                                $subscription = wcs_get_subscription($subscription->get_id());

                                                                if ($subscription) {
                                                                    $subscription->add_order_note(__('Process renewal detected that the subscription was set to requires manual renewal and '.$data_title.' data is on the subscription. Auto correction service adjusted data and processed renewal.', 'yan-co-wordpress-utility-plugin'), false, true);
                                                                    update_post_meta($subscription->get_id(), '_requires_manual_renewal', false);

                                                                    if ($is_mobilepay_subscription_and_renewal_is_less_than_72_hours == true) {
                                                                        $order->add_order_note(__('[1] Auto correction service detected MobilePay Subscription order and renewal order less than 72 hours old.', 'yan-co-wordpress-utility-plugin'), false, true);
                                                                        $auto_correct_completed = true;
                                                                    } else {
                                                                        $order->update_status('cancelled');
                                                                        $order->add_order_note(__('Auto correction service cancelled order and requested renewal process.', 'yan-co-wordpress-utility-plugin'), false, true);
                                                                        $subscription->update_status('active');
                                                                        do_action('woocommerce_scheduled_subscription_payment', $subscription->get_id());
                                                                        $auto_correct_completed = true;
                                                                    }
                                                                }
                                                            }
                                                        } else {
                                                            if ($is_mobilepay_subscription_and_renewal_is_less_than_72_hours == true) {
                                                                $order->add_order_note(__('[2] Auto correction service detected MobilePay Subscription order and renewal order less than 72 hours old.', 'yan-co-wordpress-utility-plugin'), false, true);
                                                                $auto_correct_completed = true;
                                                            } else {
                                                                // Retry renewal payment
                                                                $order->add_order_note(__('Retry renewal payment action requested by auto correction service', 'yan-co-wordpress-utility-plugin'), false, true);
                                                                do_action('woocommerce_scheduled_subscription_payment_' . wcs_get_objects_property($order, 'payment_method'), $order->get_total(), $order);

                                                                // Check that the order got completed
                                                                if ($order->get_status() == 'completed') {
                                                                    $auto_correct_completed = true;
                                                                }
                                                            }
                                                        }
                                                    }
                                                } elseif ($subscription_status == 'cancelled') {
                                                    $_schedule_end = get_post_meta($subscription->get_id(), '_schedule_end', true);
                                                    if (!empty($_schedule_end)) {
                                                        if (date('Y-m-d H:i:s') > $_schedule_end) {
                                                            $subscription->update_status('cancelled');
                                                            $auto_correct_completed = true;
                                                        }
                                                    }
                                                } elseif ($subscription_status == 'active') {
                                                    if ($subscription->get_payment_method() == 'mobilepay-subscriptions') {
                                                        // Todo: Ignore for now
                                                        $auto_correct_completed = true;
                                                    }
                                                } else {
                                                }
                                            } else {
                                                $auto_correct_completed = false;
                                            }
                                        } else {
                                            $auto_correct_completed = false;
                                        }
                                        // }
                                    }
                                } else {
                                    $auto_correct_completed = false;
                                }
                            } elseif ($action_type_id == 4) { // subscription end of prepaid term
                                $subscription = wcs_get_subscription($subscription_id);

                                if ($subscription) {
                                    $_schedule_end = get_post_meta($subscription->get_id(), '_schedule_end', true);
                                    if (!empty($_schedule_end)) {
                                        if (date('Y-m-d H:i:s') > $_schedule_end) {
                                            $subscription->update_status('cancelled');
                                            $auto_correct_completed = true;
                                        }
                                    }
                                } else {
                                    $auto_correct_completed = false;
                                }
                            } else {
                                $auto_correct_completed = false;
                            }

                            if ($auto_correct_completed == true) {
                                // We sucessfully requeued the order
                                unset($woocommerce_subscriptions_failed_scheduled_actions[$action_id]);

                                if ($testmode == false) {
                                    if (empty($woocommerce_subscriptions_failed_scheduled_actions)) {
                                        delete_option('woocommerce_subscriptions_failed_scheduled_actions');
                                    } else {
                                        update_option('woocommerce_subscriptions_failed_scheduled_actions', $woocommerce_subscriptions_failed_scheduled_actions);
                                    }
                                }
                                $results['failed_actions']['auto_correct_success']['urls'][] = $subscription_url;
                                $results['failed_actions']['auto_correct_success']['message'] .= 'Subscription ID: ' . $subscription_id . ' Action ID: ' . $action_id . ' Type ('.$action_type_id.'): ' . $action_type . '\\r\\n';
                            } else {
                                $results['failed_actions']['auto_correct_failed']['urls'][] = $subscription_url;
                                $results['failed_actions']['auto_correct_failed']['message'] .= 'Subscription ID: ' . $subscription_id . ' Action ID: ' . $action_id . ' Type ('.$action_type_id.'): ' . $action_type . '\\r\\n';
                            }

                            if ($testmode == true) {
                                $results['failed_actions']['auto_correct_success']['urls'][] = $subscription_url;
                                $results['failed_actions']['auto_correct_success']['message'] .= 'Subscription ID: ' . $subscription_id . ' Action ID: ' . $action_id . ' Type ('.$action_type_id.'): ' . $action_type . '\\r\\n';

                                $results['failed_actions']['auto_correct_failed']['urls'][] = $subscription_url;
                                $results['failed_actions']['auto_correct_failed']['message'] .= 'Subscription ID: ' . $subscription_id . ' Action ID: ' . $action_id . ' Type ('.$action_type_id.'): ' . $action_type . '\\r\\n';
                            }
                        }

                        if (empty($results['failed_actions']['auto_correct_success']['message'])) {
                            $results['failed_actions']['auto_correct_success']['message'] = 'No auto-correction actions found';
                        }

                        if (empty($results['failed_actions']['auto_correct_failed']['message'])) {
                            $results['failed_actions']['auto_correct_failed']['message'] = 'No failed auto-correction actions found';
                        }
                    }
                } else {
                    $results['failed_actions']['auto_correct_success']['message'] = 'Auto correction is disabled, you can enable it via setting ATTEMPT_AUTO_CORRECT_OF_FAILED_SUBSCRIPTIONS to true in wp-config.php ';
                    $results['failed_actions']['auto_correct_failed']['message'] = 'Auto correction is disabled, you can enable it via setting ATTEMPT_AUTO_CORRECT_OF_FAILED_SUBSCRIPTIONS to true in wp-config.php ';
                }

                $results['failed_actions']['auto_correct_success']['urls_formatted'] = implode('\\r\\n', $results['failed_actions']['auto_correct_success']['urls']);
                $results['failed_actions']['auto_correct_failed']['urls_formatted'] = implode('\\r\\n', $results['failed_actions']['auto_correct_failed']['urls']);
            // $results['failed_actions']['debug'] = '';
            } else {
                if (empty($results['failed_actions']['auto_correct_success']['message'])) {
                    $results['failed_actions']['auto_correct_success']['message'] = 'No auto-correction actions found';
                }

                if (empty($results['failed_actions']['auto_correct_failed']['message'])) {
                    $results['failed_actions']['auto_correct_failed']['message'] = 'No failed auto-correction actions found';
                }
            }

            return $results;
        }
    }
}

// add_shortcode('subtest', 'subtest');
// function subtest()
// {
//     $subscription_id = 43335; // Many
//     $subscription = wcs_get_subscription($subscription_id);

//     $related_renewal_orders = $subscription->get_related_orders('ids', 'renewal');

//     $html = '<pre>' . print_r($related_renewal_orders, true) . '</pre>';
//     $html .= '<pre>' . print_r(reset($related_renewal_orders), true) . '</pre>';

//     $subscription_id = 48827; // Singular
//     $subscription = wcs_get_subscription($subscription_id);

//     $related_renewal_orders = $subscription->get_related_orders('ids', 'renewal');

//     $html .= '<pre>' . print_r($related_renewal_orders, true) . '</pre>';
//     $html .= '<pre>' . print_r(reset($related_renewal_orders), true) . '</pre>';

//     return $html;
// }

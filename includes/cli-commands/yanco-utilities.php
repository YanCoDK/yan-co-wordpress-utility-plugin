<?php

class YanCoUtilitiesCLI
{
    public $options;
    public $options_no_exit_on_error;
    public $options_return_output;
    public $environment_types;
    public $allowed_configure_commands;
    private $astra_license_key;
    private $elementor_pro_license_key;
    private $elementor_uael_license_key;
    private $gravity_forms_license_key;
    private $acf_pro_license_key;
    private $admin_columns_pro_license_key;
    private $dynamic_content_for_elementor_key;
    private $jet_plugins_key;

    /**
     * Configure options for CLI commands
     *
     * @since  1.6.0
     * @author Yan Knudtskov
     */
    public function __construct()
    {
        $this->options = array('exit_error' => true);
        $this->options_no_exit_on_error = array('exit_error' => false);
        $this->options_return_output = array('exit_error' => false, 'return' => 'all');
        $this->environment_types = array('development', 'staging', 'production');
        $this->allowed_configure_commands = array('password-protected', 'disable-emails', 'license-keys', 'ithemes-security', 'litespeed-cache', 'antispam-bee');

        $this->astra_license_key = '74d1690d79bf362d7cf737af58b26567';
        $this->elementor_pro_license_key = 'f297ce319c4ab3e2bfa645b743ce3776';
        $this->elementor_uael_license_key = '607851a5298755165040fd1d663b61ab';
        $this->gravity_forms_license_key = '3562c5c9ed9a5c2c548816edfc5ca819';
        $this->acf_pro_license_key = 'b3JkZXJfaWQ9MzM1ODJ8dHlwZT1kZXZlbG9wZXJ8ZGF0ZT0yMDE0LTA3LTA4IDA4OjUxOjQ5';
        $this->admin_columns_pro_license_key = '03dd30ec-4046-4c27-b079-467394e533be';
        $this->dynamic_content_for_elementor_key = 'dce-a9c8bd52-10cc97dd-59f858d8';
        $this->jet_plugins_key = '404d7fdf283cf9fb34b5cb393ee3a056';
    }

    /**
     * Runs all commands need to setup development/staging/production after copying the files, database and changing credentials in wp-config.php
     *
     * @since  1.4.0
     * @author Yan Knudtskov
     */
    public function setup($args, $assoc_args)
    {
        $this->validateArgs($args, $assoc_args);
        $environment_type = $args[0];
        $perform_search_replace = false;
        $update_all = false;

        if (isset($assoc_args['search-url']) && !empty($assoc_args['search-url']) && isset($assoc_args['replace-url']) && !empty($assoc_args['replace-url'])) {
            $perform_search_replace = true;
        } else {
            $perform_search_replace = false;
            WP_CLI::line(WP_CLI::colorize('%BOnly configure defined, skipping all search-replace commands.%n'));
        }

        if (isset($assoc_args['update-all'])) {
            $update_all = true;
        }

        if ($perform_search_replace) {
            /*
            * Run search replace in the database
            */
            WP_CLI::line(WP_CLI::colorize('%BRunning search-replace..%n'));
            $search_replace_command = "search-replace '{$assoc_args['search-url']}' '{$assoc_args['replace-url']}' --all-tables";
            WP_CLI::runcommand($search_replace_command, $this->options);

            /*
            * Check for Elementor and do search replace
            */
            $elementor_installed = $this->isPluginInstalled('elementor');
            $elementor_activated = $this->isPluginActivated('elementor');
            if ($elementor_installed == true && $elementor_activated == true) {
                WP_CLI::line(WP_CLI::colorize('%BRunning Elementor replace-urls..%n'));
                $elementor_search_replace_command = "elementor replace-urls '{$assoc_args['search-url']}' '{$assoc_args['replace-url']}'";
                WP_CLI::runcommand($elementor_search_replace_command, $this->options);
            }

            /*
            * Check if this is an Enfold site and configure it
            */
            $enfold_activated = $this->isThemeActivated(array('enfold', 'enfold_child_by_yanco'));
            if ($enfold_activated == true) {
                WP_CLI::line(WP_CLI::colorize('%BEnfold detected, configuring..%n'));
                $this->configureEnfold($assoc_args['search-url'], $assoc_args['replace-url']);
            }
        }

        /*
        * Configure WP Config Environment Type
        */
        WP_CLI::line(WP_CLI::colorize('%BSetting environment type in wp-config.php..%n'));
        $this->configureWPConfig($environment_type);

        /*
        * Configure License Keys
        */
        if (!isset($assoc_args['skip-license-keys'])) {
            WP_CLI::line(WP_CLI::colorize('%BConfiguring License Keys..%n'));
            $this->activateLicenses();
        }

        if ($update_all == true) {
            WP_CLI::line(WP_CLI::colorize('%Bupdate-all defined: Updating environment to latest version.%n'));
            $this->updatePluginsCoreAndTheme();
        }

        /*
        * Development Setup
        */
        if (in_array($environment_type, array('development', 'staging'))) {
            $this->installConfigureDisableEmails();

            if (isset($assoc_args['replace-url'])) {
                WP_CLI::line(WP_CLI::colorize("%BEnable noindexing for '{$assoc_args['replace-url']}'..%n"));
            } else {
                WP_CLI::line(WP_CLI::colorize("%BEnable noindexing for site..%n"));
            }

            WP_CLI::runcommand('option set blog_public 0', $this->options_no_exit_on_error);
        }

        /*
        * Staging Setup
        */
        if (in_array($environment_type, array('staging'))) {
            $this->installConfigurePasswordProtected();
        }

        /*
        * Production Setup
        */
        if (in_array($environment_type, array('production'))) {
            $disable_emails_installed = $this->isPluginInstalled('disable-emails');
            if ($disable_emails_installed == true) {
                WP_CLI::runcommand('plugin deactivate disable-emails', $this->options_no_exit_on_error);
            }

            $password_protected_installed = $this->isPluginInstalled('password-protected');
            if ($password_protected_installed == true) {
                WP_CLI::runcommand('plugin deactivate password-protected', $this->options_no_exit_on_error);
            }

            WP_CLI::line(WP_CLI::colorize("%BEnable indexing for '{$assoc_args['replace-url']}'..%n"));
            WP_CLI::runcommand('option set blog_public 1', $this->options_no_exit_on_error);
        }

        /*
        * Reindex Yoast
        */
        if ($environment_type == 'production' && !isset($assoc_args['skip-yoast'])) {
            $this->reindexYoast();
        }

        /*
        * Antispam Bee Setup
        */
        if ($environment_type == 'production' && !isset($assoc_args['skip-antispam-bee'])) {
            $this->installConfigureAntispamBee();
        }

        /*
        * Display Environment Type Setup
        */
        // Deprecated in favor of native feature for this
        // $this->installConfigureDisplayEnvironmentType();

        /*
        * Flush Permalinks
        */
        if (!isset($assoc_args['skip-rewrite-flush'])) {
            WP_CLI::line(WP_CLI::colorize('%BFlushing permalinks cache..%n'));
            WP_CLI::runcommand('rewrite flush', $this->options);
        }

        /*
        * Flush WP Cache
        */
        if (!isset($assoc_args['skip-cache-flush'])) {
            WP_CLI::line(WP_CLI::colorize('%BFlushing cache..%n'));
            WP_CLI::runcommand('cache flush', $this->options);
        }

        if (in_array($environment_type, array('production'))) {
            WP_CLI::line(WP_CLI::colorize('%Production succesfully setup. If you\'re using LiteSpeed Cache run "wp yanco-utilities configure litespeed-cache --environment=production" %n'));
        }
    }

    /**
     * Runs specific commands to setup the requested
     *
     * @since  1.6.0
     * @author Yan Knudtskov
     */
    public function configure($args, $assoc_args)
    {
        /*
        * Validate configure type
        */
        if (!isset($args[0]) || empty($args[0])) {
            WP_CLI::error('Missing argument for what to configure, available options: ' . implode(', ', $this->allowed_configure_commands));
        }

        if (!in_array($args[0], $this->allowed_configure_commands)) {
            WP_CLI::error('Unknown argument for what to configure, available options: ' . implode(', ', $this->allowed_configure_commands));
        }

        foreach ($args as $arg) {
            if ($arg == 'password-protected') {
                if (isset($assoc_args['deactivate'])) {
                    /*
                    * Deactivate Password Protected plugin
                    */
                    WP_CLI::runcommand('plugin deactivate password-protected', $this->options);
                } else {
                    /*
                    * Install and Configure Password Protected plugin
                    */
                    $this->installConfigurePasswordProtected();
                }
            }

            if ($arg == 'disable-emails') {
                if (isset($assoc_args['deactivate'])) {
                    /*
                    * Deactivate Disable Emails plugin
                    */
                    WP_CLI::runcommand('plugin deactivate disable-emails', $this->options);
                } else {
                    /*
                    * Install and Configure Disable Emails plugin
                    */
                    $this->installConfigureDisableEmails();
                }
            }

            if ($arg == 'license-keys') {
                /*
                * Configure License Keys
                */
                WP_CLI::line(WP_CLI::colorize('%BConfiguring License Keys..%n'));
                $this->activateLicenses();
            }

            if ($arg == 'ithemes-security') {
                /*
                * Setup iThemes Security
                */
                $this->importiThemesSecurityConfig();
            }

            if ($arg == 'litespeed-cache') {
                if (isset($assoc_args['environment']) && !empty($assoc_args['environment'])) {
                    if (!in_array($assoc_args['environment'], $this->environment_types)) {
                        WP_CLI::error('Unknown environment, available options: ' . implode(', ', $this->environment_types));
                    } else {
                        $environment_type = $assoc_args['environment'];

                        /*
                        * LiteSpeed Cache Setup
                        */
                        $this->importLiteSpeedCacheConfigByEnvironmentType($environment_type);
                    }
                } else {
                    WP_CLI::error('Missing environment, available options: ' . implode(', ', $this->environment_types));
                }
            }

            if ($arg == 'antispam-bee') {
                /*
                * Antispam Bee Setup
                */
                $this->installConfigureAntispamBee();
            }
        }
    }

    /**
     * Runs search-replace commands
     *
     * @since  1.9.0
     * @author Yan Knudtskov
     */
    public function searep($assoc_args)
    {
        $this->validateSearchReplaceArgs($assoc_args);
        $perform_search_replace = false;

        if (isset($assoc_args['search-url']) && !empty($assoc_args['search-url']) && isset($assoc_args['replace-url']) && !empty($assoc_args['replace-url'])) {
            $perform_search_replace = true;
        }

        if ($perform_search_replace) {
            /*
            * Run search replace in the database
            */
            WP_CLI::line(WP_CLI::colorize('%BRunning search-replace..%n'));
            $search_replace_command = "search-replace '{$assoc_args['search-url']}' '{$assoc_args['replace-url']}' --all-tables";
            WP_CLI::runcommand($search_replace_command, $this->options);

            /*
            * Check for Elementor and do search replace
            */
            $elementor_installed = $this->isPluginInstalled('elementor');
            $elementor_activated = $this->isPluginActivated('elementor');
            if ($elementor_installed == true && $elementor_activated == true) {
                WP_CLI::line(WP_CLI::colorize('%BRunning Elementor replace-urls..%n'));
                $elementor_search_replace_command = "elementor replace-urls '{$assoc_args['search-url']}' '{$assoc_args['replace-url']}'";
                WP_CLI::runcommand($elementor_search_replace_command, $this->options);
            }

            /*
            * Check if this is an Enfold site and configure it
            */
            $enfold_activated = $this->isThemeActivated(array('enfold', 'enfold_child_by_yanco'));
            if ($enfold_activated == true) {
                WP_CLI::line(WP_CLI::colorize('%BEnfold detected, configuring..%n'));
                $this->configureEnfold($assoc_args['search-url'], $assoc_args['replace-url']);
            }

            /*
            * Flush Permalinks
            */
            WP_CLI::line(WP_CLI::colorize('%BFlushing permalinks cache..%n'));
            WP_CLI::runcommand('rewrite flush', $this->options);

            /*
            * Flush WP Cache
            */
            WP_CLI::line(WP_CLI::colorize('%BFlushing cache..%n'));
            WP_CLI::runcommand('cache flush', $this->options);
        }
    }

    /**
     * Validate arguments
     * throws a WP_CLI:error if arguments don't validate
     *
     * @since  1.9.0
     * @author Yan Knudtskov
     */
    private function validateSearchReplaceArgs($assoc_args)
    {
        /*
        * Validate search-url exists
        */
        if (isset($assoc_args['search-url']) && empty($assoc_args['search-url'])) {
            WP_CLI::error('Argument search-url is empty, add --search-url=\'SEARCH_URL\'');
        }

        /*
        * Validate format for search-url
        */
        if (isset($assoc_args['search-url']) && $this->isValidUrl($assoc_args['search-url']) == false) {
            WP_CLI::error('The provided URL: \'' . $assoc_args['search-url'] . '\' is not a valid URL.');
        }

        /*
        * Validate replace-url exists
        */
        if (isset($assoc_args['replace-url']) && empty($assoc_args['replace-url'])) {
            WP_CLI::error('Argument replace-url is empty, add --replace-url=\'REPLACE_URL\'');
        }

        /*
        * Validate format for replace-url
        */
        if (isset($assoc_args['replace-url']) && $this->isValidUrl($assoc_args['replace-url']) == false) {
            WP_CLI::error('The provided URL: \'' . $assoc_args['replace-url'] . '\' is not a valid URL.');
        }
    }

    /**
     * Validate arguments
     * throws a WP_CLI:error if arguments don't validate
     *
     * @since  1.6.0
     * @author Yan Knudtskov
     */
    private function validateArgs($args, $assoc_args)
    {
        /*
        * Validate environment type exists
        */
        if (!isset($args[0]) || empty($args[0])) {
            WP_CLI::error('Missing argument for environment type, use: development|staging|production');
        }

        /*
        * Validate environment type
        */
        if (!in_array($args[0], $this->environment_types)) {
            WP_CLI::error('Unknown environment type: \'' . $args[0] . '\'. Allowed types are: ' . implode(',', $this->environment_types));
        }

        /*
        * Validate search-url exists
        */
        if (isset($assoc_args['search-url']) && empty($assoc_args['search-url'])) {
            WP_CLI::error('Argument search-url is empty, add --search-url=\'SEARCH_URL\'');
        }

        /*
        * Validate format for search-url
        */
        if (isset($assoc_args['search-url']) && $this->isValidUrl($assoc_args['search-url']) == false) {
            WP_CLI::error('The provided URL: \'' . $assoc_args['search-url'] . '\' is not a valid URL.');
        }

        /*
        * Validate replace-url exists
        */
        if (isset($assoc_args['replace-url']) && empty($assoc_args['replace-url'])) {
            WP_CLI::error('Argument replace-url is empty, add --replace-url=\'REPLACE_URL\'');
        }

        /*
        * Validate format for replace-url
        */
        if (isset($assoc_args['replace-url']) && $this->isValidUrl($assoc_args['replace-url']) == false) {
            WP_CLI::error('The provided URL: \'' . $assoc_args['replace-url'] . '\' is not a valid URL.');
        }
    }

    /*
     * Validate URL format
     *
     * @since  1.6.0
     * @author Yan Knudtskov
     */
    private function isValidUrl($url)
    {
        // Remove all illegal characters from a url
        $santized_url = filter_var($url, FILTER_SANITIZE_URL);

        // Validate url
        if (filter_var($santized_url, FILTER_VALIDATE_URL) !== false) {
            return true;
        }

        return false;
    }

    /*
     * Check if a plugin is installed
     *
     * @since  1.6.0
     * @author Yan Knudtskov
     */
    private function isPluginInstalled($slug)
    {
        $plugin_installed = WP_CLI::runcommand('plugin is-installed ' . $slug, $this->options_return_output);
        if ($plugin_installed->return_code == 0) {
            return true;
        }

        return false;
    }

    /*
     * Check if a plugin is activated
     *
     * @since  1.6.0
     * @author Yan Knudtskov
     */
    private function isPluginActivated($slug)
    {
        $plugin_activated = WP_CLI::runcommand('plugin is-active ' . $slug, $this->options_return_output);
        if ($plugin_activated->return_code == 0) {
            return true;
        }

        return false;
    }

    /*
     * Check if a theme is activated
     *
     * @param  Array of theme slugs to check
     * @since  1.6.0
     * @author Yan Knudtskov
     */
    private function isThemeActivated($slugs)
    {
        $active_theme = WP_CLI::runcommand('theme list --status=active --field=name', $this->options_return_output);

        if (in_array($active_theme, $slugs)) {
            return true;
        }

        return false;
    }

    /*
     * Configure Enfold by generating a new Avia Dynamic CSS stylesheet and search-replacing in the enfold options
     *
     * @param  search_url, replace_url
     * @since  1.6.0
     * @author Yan Knudtskov
     */
    private function configureEnfold($search_url, $replace_url)
    {
        $avia_options_enfold = WP_CLI::runcommand('option get avia_options_enfold', $this->options_return_output);
        $avia_options_yanco = WP_CLI::runcommand('option get avia_options_yanco', $this->options_return_output);
        $avia_options_skovalfen = WP_CLI::runcommand('option get avia_options_skovalfen', $this->options_return_output);
        $enfold_options_name = '';
        $number_warnings = 0;

        if ($avia_options_enfold->return_code == 0) {
            $enfold_options_name = 'avia_options_enfold';
        }

        if ($avia_options_yanco->return_code == 0) {
            $enfold_options_name = 'avia_options_yanco';
        }

        if ($avia_options_skovalfen->return_code == 0) {
            $enfold_options_name = 'avia_options_skovalfen';
        }

        if (empty($enfold_options_name)) {
            WP_CLI::warning('Enfold detected but could not find the correct option to adjust. You will have to check the site.');
            $number_warnings++;
        } else {
            $logo_value = WP_CLI::runcommand("option pluck {$enfold_options_name} avia logo", $this->options_return_output);
            if (!empty($logo_value)) {
                $new_logo_value = str_replace($search_url, $replace_url, $logo_value);
                WP_CLI::runcommand("option patch update {$enfold_options_name} avia logo {$new_logo_value}", $this->options_no_exit_on_error);
            } else {
                WP_CLI::warning('No logo was detected. You will have to check the site.');
                $number_warnings++;
            }

            $favicon_value = WP_CLI::runcommand("option pluck {$enfold_options_name} avia favicon", $this->options_return_output);
            if (!empty($favicon_value)) {
                $new_favicon_value = str_replace($search_url, $replace_url, $favicon_value);
                WP_CLI::runcommand("option patch update {$enfold_options_name} avia favicon {$new_favicon_value}", $this->options_no_exit_on_error);
            } else {
                WP_CLI::warning('No favicon was detected. You will have to check the site.');
                $number_warnings++;
            }
        }

        if (function_exists('avia_generate_stylesheet')) {
            WP_CLI::line(WP_CLI::colorize('%BRebuilding Dynamic Avia CSS.%n'));
            if (defined('WP_CONTENT_DIR')) {
                // Todo: Maybe this is not needed
                // $path = WP_CONTENT_DIR . "/uploads/dynamic_avia";
                // $files = glob($path."/*.{css,js}", GLOB_BRACE);

                // if( !empty( $files ) ) {
                //     foreach( $files as $file ) {
                //         if(is_file($file)){
                //             unlink($file);
                //         }
                //     }
                // }

                avia_generate_stylesheet();
            } else {
                WP_CLI::warning('WP_CONTENT_DIR is not defined, cannot rebuild Dynamic Avia CSS');
                $number_warnings++;
            }
        } else {
            WP_CLI::warning('Function \'avia_generate_stylesheet()\' does not exist, cannot rebuild Dynamic Avia CSS');
            $number_warnings++;
        }

        WP_CLI::line(WP_CLI::colorize("%BEnfold configuration completed with {$number_warnings} warnings%n"));
    }

    /*
     * Configure wp-config.php
     *
     * @since  1.6.0
     * @author Yan Knudtskov
     */
    private function configureWPConfig($environment_type)
    {
        WP_CLI::runcommand("config set WP_ENVIRONMENT_TYPE {$environment_type}", $this->options);
    }

    /*
     * Update environment to latest version
     *
     * @since  1.6.0
     * @author Yan Knudtskov
     */
    private function updatePluginsCoreAndTheme()
    {
        WP_CLI::runcommand('core update', $this->options);
        WP_CLI::runcommand('plugin update --all', $this->options);
        WP_CLI::runcommand('theme update --all', $this->options);
    }

    /*
     * Activate Licenses for all relevant plugins
     *
     * @since  1.6.0
     * @author Yan Knudtskov
     */
    private function activateLicenses()
    {
        $elementor_pro_license_command_available = WP_CLI::runcommand('cli has-command "elementor-pro license activate"', $this->options_return_output);
        if ($elementor_pro_license_command_available->return_code == 0) {
            WP_CLI::runcommand("elementor-pro license activate {$this->elementor_pro_license_key}", $this->options_no_exit_on_error);
        }

        $brainstorm_force_uael_license_command_available = WP_CLI::runcommand('cli has-command "brainstormforce license activate uael"', $this->options_return_output);
        if ($brainstorm_force_uael_license_command_available->return_code == 0) {
            WP_CLI::runcommand("brainstormforce license activate uael {$this->elementor_uael_license_key}", $this->options_no_exit_on_error);
        }

        $brainstorm_force_astra_license_command_available = WP_CLI::runcommand('cli has-command "brainstormforce license activate astra-addon"', $this->options_return_output);
        if ($brainstorm_force_astra_license_command_available->return_code == 0) {
            WP_CLI::runcommand("brainstormforce license activate astra-addon {$this->astra_license_key}", $this->options_no_exit_on_error);
        }

        $gravity_forms_license_command_available = WP_CLI::runcommand('cli has-command "gf license update"', $this->options_return_output);
        if ($gravity_forms_license_command_available->return_code == 0) {
            WP_CLI::runcommand("gf license update {$this->gravity_forms_license_key}", $this->options_no_exit_on_error);
        }

        $admin_columns_pro_installed = $this->isPluginInstalled('admin-columns-pro');
        $admin_columns_pro_activated = $this->isPluginActivated('admin-columns-pro');

        if ($admin_columns_pro_installed == true && $admin_columns_pro_activated == true) {
            WP_CLI::runcommand("config set ACP_LICENCE {$this->admin_columns_pro_license_key}", $this->options_no_exit_on_error);
        }

        $advanced_custom_fields_pro_installed = $this->isPluginInstalled('advanced-custom-fields-pro');
        $advanced_custom_fields_pro_activated = $this->isPluginActivated('advanced-custom-fields-pro');

        if ($advanced_custom_fields_pro_installed == true && $advanced_custom_fields_pro_activated == true) {
            // Loads ACF plugin
            include_once ABSPATH . 'wp-content/plugins/advanced-custom-fields-pro/acf.php';

            // connect
            $post = [
                'acf_license' => $this->acf_pro_license_key,
                'acf_version' => acf_get_setting('version'),
                'wp_name'     => get_bloginfo('name'),
                'wp_url'      => home_url(),
                'wp_version'  => get_bloginfo('version'),
                'wp_language' => get_bloginfo('language'),
                'wp_timezone' => get_option('timezone_string'),
            ];

            // connect
            $response = \acf_updates()->request('v2/plugins/activate?p=pro', $post);

            // ensure response is expected JSON array (not string)
            if (is_string($response)) {
                WP_CLI::error('Server Error when trying to activate Advanced Custom Fields License: ' . esc_html($response));
            }

            // error
            if (is_wp_error($response)) {
                WP_CLI::error('Reponse Error when trying activate Advanced Custom Fields License: ' . $response->get_error_message());
            }

            // success
            if ($response['status'] == 1) {
                \acf_pro_update_license($response['license']); // update license
                \acf_updates()->refresh_plugins_transient();
                WP_CLI::line(WP_CLI::colorize("%BAdvanced Custom Fields license activated.%n"));
                WP_CLI::line('ACF PRO: ' . wp_strip_all_tags($response['message'])); // show message
            } else {
                WP_CLI::warning('Advanced Custom Fields License was not activated. You will have to activate it manually.');
            }
        }

        $dynamic_content_elementor_installed = $this->isPluginInstalled('dynamic-content-for-elementor');
        $dynamic_content_elementor_activated = $this->isPluginActivated('dynamic-content-for-elementor');

        if ($dynamic_content_elementor_installed == true && $dynamic_content_elementor_activated == true) {
            // Activate Dynamic Content for Elementor
            if (class_exists('DynamicContentForElementor\Dashboard\License')) {
                WP_CLI::runcommand('option update dce_license_key ' . $this->dynamic_content_for_elementor_key, $this->options_no_exit_on_error);
                $res = \DynamicContentForElementor\LicenseSystem::call_api('activate', $this->dynamic_content_for_elementor_key, false);
                $wpurl = get_option('siteurl');
                $dce_instance = str_replace('https://', '', $wpurl);
                $dce_instance = str_replace('http://', '', $wpurl);

                if ($res) {
                    $dce_license_domain = base64_encode($dce_instance);
                    WP_CLI::runcommand('option update dce_license_activated 1', $this->options);
                    WP_CLI::runcommand('option update dce_license_domain ' . $dce_license_domain, $this->options);
                    WP_CLI::line(WP_CLI::colorize("%BDynamic Content for Elementor Activated.%n"));
                } else {
                    WP_CLI::runcommand('option update dce_license_activated 0', $this->options);
                }
            }
        }

        // Activate Jet Plugins
        $active_plugins = get_option('active_plugins');
        foreach ($active_plugins as $active_plugin) {
            if (strpos($active_plugin, 'jet-') !== false) {
                WP_CLI::line(WP_CLI::colorize("%Y " . $active_plugin . " is installed and active. Activate the license through the Dashboard > Settings > Yan&Co Utilities as Crocoblocks does not support WP CLI.%n"));
                break;
            }
        }

        // Activate WCAM License
        if (class_exists('WC_AM_Client_2_8_1')) {
            WP_CLI::line(WP_CLI::colorize("%YActivate the license for Yan&Co Wordress Utilities through the Dashboard > Settings > Yan&Co Utilities.%n"));
        }
    }

    /*
     * Reindex Yoast if available
     *
     * @since  1.6.0
     * @author Yan Knudtskov
     */
    private function reindexYoast()
    {
        $yoast_index_command_available = WP_CLI::runcommand('cli has-command "yoast index"', $this->options_return_output);
        if ($yoast_index_command_available->return_code == 0) {
            WP_CLI::runcommand("yoast index --reindex --skip-confirmation", $this->options_no_exit_on_error);
        }
    }

    /*
    * Import iThemes Security Config if commands is available
    *
    * @since  1.6.0
    * @author Yan Knudtskov
    */
    private function importiThemesSecurityConfig()
    {
        $ithemes_security_active = $this->isPluginActivated('ithemes-security-pro');
        if ($ithemes_security_active == true) {
            $ithemes_security_import_export_command_available = WP_CLI::runcommand('cli has-command "itsec import-export import"', $this->options_return_output);
            if ($ithemes_security_import_export_command_available->return_code == 0) {
                WP_CLI::runcommand("itsec import-export import " . YANCO_UTILITY_PLUGIN_DIR_PATH . '/assets/configs/ithemes-security-pro/itsec_options.zip', $this->options_no_exit_on_error);
                WP_CLI::line(WP_CLI::colorize("%BiThemes Security configured from import file.%n"));
            } else {
                WP_CLI::warning('iThemes Security was not configured');
            }
        } else {
            WP_CLI::warning('iThemes Security was not active, cannot be configured');
        }
    }

    /*
    * Setup LiteSpeed Cache Based on Environment
    *
    * @since  1.6.0
    * @author Yan Knudtskov
    */
    private function importLiteSpeedCacheConfigByEnvironmentType($environment_type)
    {
        $litespeed_installed = $this->isPluginInstalled('litespeed-cache');
        $litespeed_activated = $this->isPluginActivated('litespeed-cache');
        if ($litespeed_installed == true && $litespeed_activated == true) {
            WP_CLI::line(WP_CLI::colorize("%BFound that LiteSpeed Cache is activated, configuring..%n"));

            if ($environment_type == 'production') {
                /*
                * Only configure for production
                */
                WP_CLI::runcommand("litespeed-option import " . YANCO_UTILITY_PLUGIN_DIR_PATH . '/assets/configs/litespeed-cache/LSCWP_cfg.data', $this->options_no_exit_on_error);
            }

            if ($environment_type == 'staging' || $environment_type == 'development') {
                WP_CLI::line(WP_CLI::colorize("%BDisabling Cloudflare%n"));
                WP_CLI::runcommand('litespeed-option set cdn-cloudflare 0', $this->options_no_exit_on_error);
                WP_CLI::line(WP_CLI::colorize("%BDisabling Object Cache%n"));
                WP_CLI::runcommand('litespeed-option set object 0', $this->options_no_exit_on_error);
                WP_CLI::runcommand('litespeed-option set object-host \'\'', $this->options_no_exit_on_error);
            }

            WP_CLI::line(WP_CLI::colorize("%BExcluding cookie for Password Protected plugin%n"));
            WP_CLI::runcommand('option patch update litespeed.conf.cache-exc_cookies 0 bid_1_password_protected_auth', $this->options_no_exit_on_error);

            WP_CLI::line(WP_CLI::colorize("%BPurging LiteSpeed Cache%n"));
            WP_CLI::runcommand('litespeed-purge all', $this->options_no_exit_on_error);
        } else {
            WP_CLI::line(WP_CLI::colorize("%BFound that LiteSpeed Cache is not activated, skipping configuration..%n"));
        }
    }

    /*
    * Install and Configure Disable Emails plugin
    *
    * @since  1.6.0
    * @author Yan Knudtskov
    */
    private function installConfigureDisableEmails()
    {
        WP_CLI::line(WP_CLI::colorize('%BSetting up Disable Emails plugin..%n'));
        $disable_emails_installed = $this->isPluginInstalled('disable-emails');
        if ($disable_emails_installed) {
            $disable_emails_activated = $this->isPluginActivated('disable-emails');
            if ($disable_emails_activated == false) {
                WP_CLI::runcommand('plugin activate disable-emails', $this->options);
            }
        } else {
            WP_CLI::runcommand('plugin install disable-emails', $this->options);
            WP_CLI::runcommand('plugin activate disable-emails', $this->options);
        }
    }

    /*
    * Install and Configure Password Protected plugin
    *
    * @since  1.6.0
    * @author Yan Knudtskov
    */
    private function installConfigurePasswordProtected()
    {
        WP_CLI::line(WP_CLI::colorize('%BSetting up Password Protected plugin..%n'));

        $password_protected_installed = $this->isPluginInstalled('password-protected');
        if ($password_protected_installed) {
            $password_protected_activated = $this->isPluginActivated('password-protected');
            if ($password_protected_activated == false) {
                WP_CLI::runcommand('plugin activate password-protected', $this->options);
            }
        } else {
            WP_CLI::runcommand('plugin install password-protected', $this->options);
            WP_CLI::runcommand('plugin activate password-protected', $this->options);
        }

        WP_CLI::line(WP_CLI::colorize('%BSetting up Password Protected settings..%n'));
        WP_CLI::runcommand('option set password_protected_status 1', $this->options_no_exit_on_error);
        WP_CLI::runcommand('option set password_protected_administrators 1', $this->options_no_exit_on_error);
        WP_CLI::line(WP_CLI::colorize('%BSetting up Password Protected password: 1234..%n'));
        WP_CLI::runcommand('option set password_protected_password 81dc9bdb52d04dc20036dbd8313ed055', $this->options_no_exit_on_error);
    }

    /*
    * Install and Configure Password Protected plugin
    *
    * @since  1.8.0
    * @author Yan Knudtskov
    */
    private function installConfigureAntispamBee()
    {
        WP_CLI::line(WP_CLI::colorize('%BSetting up Antispam Bee plugin..%n'));

        $antispam_bee_installed = $this->isPluginInstalled('antispam-bee');
        if ($antispam_bee_installed) {
            $antispam_bee_activated = $this->isPluginActivated('antispam-bee');
            if ($antispam_bee_activated == false) {
                WP_CLI::runcommand('plugin activate antispam-bee', $this->options);
            }
        } else {
            WP_CLI::runcommand('plugin install antispam-bee', $this->options);
            WP_CLI::runcommand('plugin activate antispam-bee', $this->options);
        }

        WP_CLI::line(WP_CLI::colorize('%BSetting up Antispam Bee settings..%n'));
        WP_CLI::runcommand('option patch update antispam_bee flag_spam 1', $this->options_no_exit_on_error);
        WP_CLI::runcommand('option patch update antispam_bee always_allowed 1', $this->options_no_exit_on_error);
    }

    /*
    * Install and Configure Display Environment Type plugin
    *
    * @since  2.1.0
    * @author Yan Knudtskov
    */
    private function installConfigureDisplayEnvironmentType()
    {
        WP_CLI::line(WP_CLI::colorize('%BSetting up Display Environment Type plugin..%n'));

        $display_environment_type_installed = $this->isPluginInstalled('display-environment-type');
        if ($display_environment_type_installed) {
            $display_environment_type_activated = $this->isPluginActivated('display-environment-type');
            if ($display_environment_type_activated == false) {
                WP_CLI::runcommand('plugin activate display-environment-type', $this->options);
            }
        } else {
            WP_CLI::runcommand('plugin install display-environment-type', $this->options);
            WP_CLI::runcommand('plugin activate display-environment-type', $this->options);
        }
    }
}

/*
 * Registers our command when cli get's initialized.
 *
 * @since  1.4.0
 * @author Yan Knudtskov
 */
function yanco_utilities_cli_register_commands()
{
    WP_CLI::add_command('yanco-utilities', 'YanCoUtilitiesCLI');
}

add_action('cli_init', 'yanco_utilities_cli_register_commands');

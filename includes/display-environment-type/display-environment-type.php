<?php

/**
 * Adds the "at a glance" item.
 *
 * @param array $items The current "at a glance" items.
 *
 * @return array The updated "at a glance" items array.
 */
function yanco_det_add_glance_item($items)
{
    $env_type = wp_get_environment_type();

    if (!empty($env_type)) {
        $items[] = '<span class="' . esc_attr('det-env-type det-' . $env_type) . '" title="' .  esc_attr__('Environment Type', 'display-environment-type') . '">' . esc_html(ucfirst($env_type)) . '</span>';
    }

    return $items;
}

/**
 * Adds an admin bar item.
 *
 * @param \WP_Admin_Bar $admin_bar The WordPress toolbar.
 *
 * @return void
 */
function yanco_det_add_toolbar_item($admin_bar)
{
    $env_type = wp_get_environment_type();

    if (!empty($env_type)) {
        $admin_bar->add_menu(array(
            'id'    => 'det_env_type',
            'parent'=> 'top-secondary',
            'title' => '<span class="ab-icon"></span><span class="ab-label">' . esc_html(ucfirst($env_type)) . '</span>',
            'meta'  => array(
                'title' => __('Environment Type', 'display-environment-type'),
                'class' => 'det-' . sanitize_title($env_type),
            ),
        ));
    }
}

/**
 * Determine whether or not to display the environment type.
 *
 * @return bool Whether the plugin should display anything.
 */
function yanco_det_should_display()
{
    // By default, we don't display anything.
    $display = false;

    // If the function doesn't exist, the plugin absolutely cannot function.
    if (! function_exists('wp_get_environment_type')) {
        return false;
    }

    // If the admin bar is not showing there is no place to display the environment type.
    if (! is_admin_bar_showing()) {
        return false;
    }

    if (is_admin()) {
        // Display in wp-admin for any role above subscriber.
        if (is_user_logged_in() && current_user_can('edit_posts')) {
            $display = true;
        }
    } elseif (is_user_logged_in() && current_user_can('manage_options')) {
        // Display on the front-end only if user has the manage_options capability.
        $display = true;
    }

    /**
     * Filter whether or not the environent type should be displayed.
     *
     * Allows you to perform checks like user capabilities or is_admin()
     * and return true to display the environment type, or false to not.
     *
     * @since 1.2
     *
     * @param boolean $display Whether the environment type should be displayed.
     */
    $display = (bool) apply_filters('det_display_environment_type', $display);

    return $display;
}
function yanco_det_enqueue_styles()
{
    wp_enqueue_style('yanco-det', plugin_dir_url(dirname(dirname(__DIR__)) . '/display-environment-type.php') . 'includes/display-environment-type/css/admin.css');
}

<?php

/*
 * Remove WordPress Dashboard Widgets
 */
add_action('wp_dashboard_setup', 'yanco_remove_dashboard_widgets', 100000);
function yanco_remove_dashboard_widgets()
{
    remove_meta_box('dashboard_primary', 'dashboard', 'side'); // WordPress.com Blog
    // remove_meta_box( 'dashboard_plugins','dashboard','normal' ); // Plugins
    remove_meta_box('dashboard_right_now', 'dashboard', 'normal'); // Right Now / At a Glance
    remove_action('welcome_panel', 'wp_welcome_panel'); // Welcome Panel
    // remove_action( 'try_gutenberg_panel', 'wp_try_gutenberg_panel'); // Try Gutenberg
    remove_meta_box('dashboard_quick_press', 'dashboard', 'side'); // Quick Press widget
    remove_meta_box('dashboard_recent_drafts', 'dashboard', 'side'); // Recent Drafts
    remove_meta_box('dashboard_secondary', 'dashboard', 'side'); // Other WordPress News
    remove_meta_box('dashboard_incoming_links', 'dashboard', 'normal'); //Incoming Links
    // remove_meta_box('rg_forms_dashboard','dashboard','normal'); // Gravity Forms
    // remove_meta_box('dashboard_recent_comments','dashboard','normal'); // Recent Comments
    // remove_meta_box('icl_dashboard_widget','dashboard','normal'); // Multi Language Plugin
    remove_meta_box('dashboard_activity', 'dashboard', 'normal'); // Activity
    remove_meta_box('e-dashboard-overview', 'dashboard', 'normal'); // Elementor Overview
    remove_meta_box('dce-dashboard-overview', 'dashboard', 'normal'); // Dynamic Content for Elementor Overview
    remove_meta_box('searchwp_statistics', 'dashboard', 'normal'); // Search WP Statistics
    // remove_meta_box('wpseo-dashboard-overview', 'dashboard', 'normal'); // Yoast SEO Post Overview
    remove_meta_box('aaaa_webappick_latest_news_dashboard_widget', 'dashboard', 'side'); // Latest News from WebAppick Blog

    remove_meta_box('quadlayers-dashboard-overview', 'dashboard', 'side'); // QuadLayers News
    remove_meta_box('quadlayers-dashboard-overview', 'dashboard', 'normal'); // QuadLayers News

    remove_meta_box('redux_dashboard_widget', 'dashboard', 'side'); // Redux Framework News

    remove_meta_box('woo_vm_status_widget', 'dashboard', 'normal'); // Plugin nyheder - af Visser Labs
    remove_meta_box('woo_vl_news_widget', 'dashboard', 'normal'); // Visser Labs Plugins

    remove_meta_box('wp_mail_smtp_reports_widget_lite', 'dashboard', 'normal'); // WP Mail SMTP

    remove_meta_box('wpp_trending_dashboard_widget', 'dashboard', 'normal'); // WP Popular Posts
    remove_meta_box('wpp_trending_dashboard_widget', 'dashboard', 'side'); // WP Popular Posts

    remove_meta_box('mc_mm_dashboard_widget', 'dashboard', 'normal'); // MailChimp Forms by MailMunch
    remove_meta_box('mc_mm_dashboard_widget', 'dashboard', 'side'); // MailChimp Forms by MailMunch

    remove_meta_box('wpforms_reports_widget_lite', 'dashboard', 'normal'); // WPForms
    remove_meta_box('wpforms_reports_widget_lite', 'dashboard', 'side'); // WPForms

    remove_meta_box('wp-dashboard-widget-news', 'dashboard', 'normal'); // WPForms
    remove_meta_box('wp-dashboard-widget-news', 'dashboard', 'side'); // WPForms

    remove_meta_box('hasthemes-dashboard-stories', 'dashboard', 'normal'); // HasThemes
    remove_meta_box('hasthemes-dashboard-stories', 'dashboard', 'side'); // HasThemes

}

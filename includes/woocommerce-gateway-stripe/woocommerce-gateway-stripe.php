<?php

add_action('woocommerce_admin_order_data_after_billing_address', 'yanco_woocommerce_admin_order_data_after_billing_address_display_stripe_data', 10, 1);
function yanco_woocommerce_admin_order_data_after_billing_address_display_stripe_data($order)
{
    $payment_method = get_post_meta($order->get_id(), '_payment_method', true);
    $post_type = get_post_type($order->get_id());

    if ($payment_method !== 'stripe') {
        return;
    }

    $stripe_customer_id = get_post_meta($order->get_id(), '_stripe_customer_id', true);
    $stripe_source_id = get_post_meta($order->get_id(), '_stripe_source_id', true);

    if ($stripe_customer_id != false) {
        $stripe_customer_id_link = 'https://dashboard.stripe.com/customers/' . $stripe_customer_id;
        echo '<p><strong>Stripe Customer Link:</strong><br>
        <a target="_blank" rel="noopener" href="' . $stripe_customer_id_link . '">' . $stripe_customer_id_link . '</a></p>';
    }

    if ($stripe_source_id != false) {
        $stripe_source_id_link = 'https://dashboard.stripe.com/sources/' . $stripe_source_id;
        echo '<p><strong>Stripe Sources Link:</strong><br>
        <a target="_blank" rel="noopener" href="' . $stripe_source_id_link . '">' . $stripe_source_id_link . '</a></p>';
    }

    if ($post_type != 'shop_order') {
        return;
    }

    $stripe_intent_id = get_post_meta($order->get_id(), '_stripe_intent_id', true);
    if ($stripe_intent_id != false) {
        $stripe_intent_id_link = 'https://dashboard.stripe.com/payments/' . $stripe_intent_id;
        echo '<p><strong>Stripe Payment Intent Link:</strong><br>
        <a target="_blank" rel="noopener" href="' . $stripe_intent_id_link . '">' . $stripe_intent_id_link . '</a></p>';
    } else {
        echo '<p><strong>Stripe Payment Intent Link:</strong><br>
        NO STRIPE PAYMENT INTENT ID FOUND!</p>';
    }

    $transaction_id = get_post_meta($order->get_id(), '_transaction_id', true);
    if ($transaction_id != false) {
        $transaction_id_link = 'https://dashboard.stripe.com/payments/' . $transaction_id;
        echo '<p><strong>Stripe Transaction Link:</strong><br>
        <a target="_blank" rel="noopener" href="' . $transaction_id_link . '">' . $transaction_id_link . '</a></p>';
    } else {
        echo '<p><strong>Stripe Transaction Link:</strong><br>
        NO STRIPE PAYMENT TRANSACTION ID FOUND!</p>';
    }
}
